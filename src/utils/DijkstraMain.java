package utils;

import java.util.ArrayList;
import java.util.Vector;

import bean.Datas;
import bean.NextNode;
import bean.Node;
import bean.Server;

/**
 * 工具类，用于获取两节点间的最短路径和路径长度
 * <br>同时每次获取路径时更新该对象中的nodes成员和server成员；
 * <br>通过getNodes()和getServer()方法可以获取更新后的成员；
 * <br>通过getDistance()方法可以获取路径长度，返回值为整型。
 * @author-team3
 **/
public class DijkstraMain {

	/**
	 * 邻接表的引用，通过nodes可访问整张图的所有信息。若图不存在，则nodes为null。
	 */
	private ArrayList<Node> nodes = null;

	public ArrayList<Node> getNodes() {
		return nodes;
	}

	/**
	 * 邻接表的引用,基站邻接表
	 */
	private Server server = null;

	public Server getServer() {
		return server;
	}

	/**
	 * 源结点和目标结点。
	 */
	private int sourceNode, targetNode;

	/**
	 * 图中总的结点数。
	 */
	int totalNodeNum;


	/**
	 * 两结点间的路径长度。
	 */
	private int distance = -1;

	/**
	 * 求得最短路径子图中的每个结点的前驱结点。用于求出从源结点到目标结点的一条通路。
	 */
	int preNodes[] = null;

	/**
	 * 是否存在可行的通路,true为存在，false为不存在
	 */
	boolean state = true;

	/**
	 * 是否已经进行广播耗电，是为true，否为false
	 */
	boolean hasReduce[] = null;

	/**
	 * 储存不足以完成三次握手的结点的权值
	 */
	Vector<Object> queue = new Vector<Object>();

	public DijkstraMain(Datas datas){
		this.nodes = datas.getNodes();
		this.server = datas.getServer();
		this.totalNodeNum = nodes.size()+1;
		
		
//		hasReduce = new boolean[totalNodeNum];
//		for(int i = 0;i<totalNodeNum;i++){
//			hasReduce[i] = false;
//		}
	}

	/**
	 * 返回所求结点间最短路径经过的结点序列。其中，path[0]为目标结点。
	 * @param startID
	 * 起始结点编号
	 * @param endID
	 * 目的结点编号
	 * @return path[]
	 * 所求结点间最短路径经过的结点序列
	 */
	public int[] getPath(int startID,int endID) {
		
		this.sourceNode = startID;
		this.targetNode = endID;
		Node nodeServer = new Node(server.getNum(),server.getX(),server.getY(),server.getRadius(),
				server.getPower(),server.getPower_consumption());
		nodeServer.setFirstNode(server.getFirstNode());
		this.nodes.add(0,nodeServer);
		preNodes = new int[totalNodeNum];
		for (int i = 0; i < totalNodeNum; i++) {
			preNodes[i] = sourceNode;
		}
		//电量检查，排除无效节点
		//powerCheck();
		//广播耗电
		//powerReduceBroadcast(startID);
		//确定可行通路，至少可以执行一次完整数据传输
		go();
		int path[]=null;
		if(!state){path = null;}else{
			int temp = targetNode;
			int nodeNumInPath = 1; 
			while (temp != sourceNode) {
				nodeNumInPath++;
				temp = preNodes[temp];
			}

			path = new int[nodeNumInPath];		
			path[0] = targetNode;
			temp = targetNode;
			int nextPathNode = 1;
			while (temp != sourceNode) {
				temp = preNodes[temp];
				path[nextPathNode] = temp;
				nextPathNode++;
			}
			
			this.nodes.remove(0);
		}
//		//将不足以完成一次完整通讯但仍有电量的节点重新激活（暂定，可能存在逻辑错误）
//		int i=1;
//		if(server.getPower()<8*server.getPower_consumption()&&server.isPower_reduce()){
//			i=0;
//		}       
//		for(;i<nodes.size();i++){
//			NextNode getback = nodes.get(i).firstNode;
//			if(nodes.get(i).getPower()<8*nodes.get(i).getPowerConsumption()&&nodes.get(i).isState()){
//				while (getback != null) {
//					if (queue.get(0) instanceof Integer) {
//						getback.edgeWeight = ((Integer)queue.remove(0)).intValue();
//						getback = getback.nextNode;
//					}
//				}
//			}
//		}
//		if(nodes.get(targetNode).getPower()<8*server.getPower_consumption()||!nodes.get(targetNode).isState()){
//			path=null;
//		}
//		//完成三次握手耗电操作
//		if(path!=null)
//		{powerReduceTWH(path);}
		return path;
	}

	/**
	 * 检查电量，如果电量不足以完成工作则将其邻接表置空
	 */
	public void powerCheck(){
		int i=1;
		if(server.getPower()<5*server.getPower_consumption()&&server.isPower_reduce()){
			i=0;
		}           
		for( ;i<nodes.size();i++){
			//要求至少能完成一次完整数据传输，否则从连接图中暂时移除(此处默认为完成连接并能成功进行一次数据传输，不包括正常断开连接操作)
			if(nodes.get(i).getPower()<5*nodes.get(i).getPowerConsumption()
					&&nodes.get(i).getPower()>nodes.get(i).getPowerConsumption()
					&&nodes.get(i).isState()){ 
				NextNode temp1 = nodes.get(i).firstNode;		
				while (temp1 != null) {
					queue.add(new Integer(temp1.edgeWeight));
					temp1.edgeWeight = 0X0FFFFFFF;
					temp1 = temp1.nextNode;
				}
			}   
			if(nodes.get(i).getPower()<nodes.get(i).getPowerConsumption()||!nodes.get(i).isState()){
				nodes.get(i).setState(false);
				NextNode temp2 = nodes.get(i).firstNode;		     
				while (temp2 != null) {
					temp2.edgeWeight = 0X0FFFFFFF;
					temp2 = temp2.nextNode;
				}
			}
		}	
	}

	/**
	 * 功耗方法，针对三次握手的电量消耗 (暂定，可能存在逻辑错误)
	 * @param path[]
	 *        可行路径
	 */
	public void powerReduceTWH(int path[]){
		//三次握手第一次在广播时耗电，此处只需处理剩余俩次操作耗电
		for(int i=0;i<path.length;i++){
			//当路径中含有基站时对基站进行操作
			if(path[i]==0){
				if(server.isPower_reduce()){
					server.setPower(server.getPower()-2*server.getPower_consumption());
					nodes.get(path[i]).setPower(nodes.get(path[i]).getPower()-2*nodes.get(path[i]).getPowerConsumption());
				}
			}else{
				nodes.get(path[i]).setPower(nodes.get(path[i]).getPower()-2*nodes.get(path[i]).getPowerConsumption());
			}
		}
	}
	/**
	 * 功耗方法，针对广播的电量消耗 (暂定，可能存在逻辑错误)
	 * @param startID
	 *        源节点
	 */
	public void powerReduceBroadcast(int startID){
		//查看当前是否为可用节点
		if(nodes.get(startID).isState()){
			//若当前广播节点为基站，且基站允许耗电操作
			if(startID == 0&&server.isPower_reduce()){
				server.setPower(server.getPower()-server.getPower_consumption());
				nodes.get(startID).
				setPower(nodes.get(startID).getPower()-nodes.get(startID).getPowerConsumption());
			}else{
				nodes.get(startID).
				setPower(nodes.get(startID).getPower()-nodes.get(startID).getPowerConsumption());
			}
			hasReduce[startID] = true;
			NextNode temp = null;
			temp = nodes.get(startID).firstNode;
			while(temp!=null){
				if(hasReduce[temp.nodeNum]==false){
					powerReduceBroadcast(temp.nodeNum);
				}
				temp = temp.nextNode;
			}	
		}

	}

	/**
	 * 进行最短路径算法
	 */
	public void go() {  
		//用于存储从源结点到其他各个结点的最短路径长度的temp。
		int distanceTemp[] = new int[totalNodeNum];
		//初始时将从源结点到其他各个结点的最短路径长度全都设置为无穷远。除源结点到自身的长度为0。
		for (int i = 0; i < totalNodeNum; i++) {
			distanceTemp[i] = 0X0FFFFFFF;
		}
		distanceTemp[sourceNode] = 0;

		//根据图的邻接表具有的信息，将于源结点邻接的结点的路径长度设置为边的权值，其余不变，作为长度初始值。
		NextNode temp = null;
		temp = nodes.get(sourceNode).firstNode;
		//源节点无邻接表,反馈路径不可用
		if(temp==null){state = false;}else{
			distanceTemp[temp.nodeNum] = temp.edgeWeight;
			while (temp.nextNode != null) {
				temp = temp.nextNode;
				distanceTemp[temp.nodeNum] = temp.edgeWeight;
			}

			//已经找到的在最短路径子图中的结点i对应的hasInPath[i]设置为true，其余设置为false。初始值全为false。
			boolean hasInPath[] = new boolean[totalNodeNum];
			for (int i = 0; i < totalNodeNum; i++) {
				hasInPath[i] = false;
			}

			//nextNode为所要找的下一个到源结点路径最短的结点。
			int nextNode = -1;
			//当找到的结点不是目标结点时，继续找下一个结点。
			while (nextNode != targetNode) {
				//下一个结点为不在已找到的最短路径子图中的结点中离源结点路径最短的结点。
				nextNode = chooseNextPathNode(distanceTemp, hasInPath);
				//将此结点加入到最短路径子图中。
				if(nextNode==-1){state = false;break;}else{
					hasInPath[nextNode] = true;
					//通过新找到的结点，比较剩余结点结点到源结点的路径。
					//若源结点通过新结点到剩余某结点的总路径小于目前源结点到某结点的路径，则修改源结点到某结点的路径。
					updateDistance(nextNode, distanceTemp, hasInPath);
				}
				//将最终从源结点到目标结点的路径长度赋给distance。
				distance = distanceTemp[targetNode];
			}
		}
	}

	/**
	 * 找下一个结点。<br>下一个结点为不在已找到的最短路径子图中的结点中离源结点路径最短的结点。
	 * @param distanceTemp
	 * 目前从源结点到各结点的距离
	 * @param hasInPath
	 * 已经在最短路径子图中的结点
	 * @return nextNode
	 * 找到的下一个结点的索引。
	 */
	private int chooseNextPathNode(int distanceTemp[], boolean hasInPath[]) {
		int temp = 0X0FFFFFFF;
		int nextNode = -1;
		for (int i = 0; i < totalNodeNum; i++) {
			if (!hasInPath[i]) {
				if (temp > distanceTemp[i]) {
					temp = distanceTemp[i];
					nextNode = i;
				}
			}
		}
		return nextNode;
	}

	/**
	 * 通过新找到的结点，比较剩余结点结点到源结点的路径。
	 * <br>若源结点通过新结点到剩余某结点的总路径小于目前源结点到某结点的路径，则修改源结点到某结点的路径。
	 * @param currentNode
	 * 刚刚找到的到源结点路径最短的结点的索引
	 * @param distanceTemp
	 * 目前从源结点到各结点的距离
	 * @param hasInPath
	 * 已经在最短路径子图中的结点
	 */
	private void updateDistance(int currentNode, int distanceTemp[], boolean hasInPath[]) {
		NextNode temp = nodes.get(currentNode).firstNode;

		while (temp != null) {
			if (!hasInPath[temp.nodeNum]) {
				if (distanceTemp[currentNode] + temp.edgeWeight < distanceTemp[temp.nodeNum]) {
					distanceTemp[temp.nodeNum] = distanceTemp[currentNode] + temp.edgeWeight;
					preNodes[temp.nodeNum] = currentNode;
				}
			}

			temp = temp.nextNode;
		}
	}

	/**
	 * 返回所求最短路径的路径长度。
	 * @return distance
	 * 所求路径的总路径长度
	 */
	public int getDistance() {
		return distance;
	}
}
