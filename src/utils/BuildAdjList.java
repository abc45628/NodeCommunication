package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import bean.Node;
import bean.Server;

/**
 * 工具类，用于生成nodes和server的邻接表 <br>
 **/
public class BuildAdjList {
	
	/**
	 * 生成结点邻接表(即每个点获取其半径以内的所有的点的 )
	 */
	public static void toAdjNodes(ArrayList<Node> nodes, Server server) {
		for (Node nodeCurrent : nodes) {
			//判断节点与基站是否为邻接节点，是则加入节点s邻接表
			int dvalueServer = (int) (Math.pow((nodeCurrent.getX() - server.getX()), 2)
					+ Math.pow((nodeCurrent.getY() - server.getY()), 2));
			dvalueServer = (int) Math.sqrt(dvalueServer);
			if (dvalueServer <= nodeCurrent.getRadius()) {
				nodeCurrent.getNodes().add(server.getNum());
				nodeCurrent.getDvalues().put(server.getNum(), dvalueServer);
			}
			
			//遍历所有节点，并将半径以内的节点加入邻接表
			for (Node nodeTemp : nodes) {
				if (nodeCurrent.getNum() == nodeTemp.getNum()) {} else {
					int dvalue = (int) (Math.pow((nodeCurrent.getX() - nodeTemp.getX()), 2)
							+ Math.pow((nodeCurrent.getY() - nodeTemp.getY()), 2));
					dvalue = (int) Math.sqrt(dvalue);
					if (dvalue <= nodeCurrent.getRadius()) {
						nodeCurrent.getNodes().add(nodeTemp.getNum());
						nodeCurrent.getDvalues().put(nodeTemp.getNum(), dvalue);
					}
				}
			}
		}
		toAdjServer(nodes,server);
	}
	
	/**
	 * 生成基站邻接表
	 */
	private static void toAdjServer(ArrayList<Node> nodes, Server server) {
		for (Node node : nodes) {
			int dvalueServer = (int) (Math.pow((node.getX() - server.getX()), 2)
					+ Math.pow((node.getY() - server.getY()), 2));
			dvalueServer = (int) Math.sqrt(dvalueServer);
			if (dvalueServer <= server.getRadius()) {
				server.getNodes().add(node.getNum());
				server.getDvalues().put(node.getNum(), dvalueServer);
			}
		}
	}
	
	/**
	 * 生成结点邻接表(即每个点获取其半径以内的所有的点的 )
	 */
	public static void toAdjNodes(List<Node> nodes, Server server,List<Integer> mNodes) {
		for (Node nodeCurrent : nodes) {
			if(!mNodes.contains(nodeCurrent.getNum())){
				//判断节点与基站是否为邻接节点，是则加入节点s邻接表
				int dvalueServer = (int) (Math.pow((nodeCurrent.getX() - server.getX()), 2)
						+ Math.pow((nodeCurrent.getY() - server.getY()), 2));
				dvalueServer = (int) Math.sqrt(dvalueServer);
				if (dvalueServer <= nodeCurrent.getRadius()) {
					nodeCurrent.getNodes().add(server.getNum());
					nodeCurrent.getDvalues().put(server.getNum(), dvalueServer);
				}
				
				//遍历所有节点，并将半径以内的节点加入邻接表
				for (Node nodeTemp : nodes) {
					if(!mNodes.contains(nodeTemp.getNum())){
						if (nodeCurrent.getNum() == nodeTemp.getNum()) {} else {
							int dvalue = (int) (Math.pow((nodeCurrent.getX() - nodeTemp.getX()), 2)
									+ Math.pow((nodeCurrent.getY() - nodeTemp.getY()), 2));
							dvalue = (int) Math.sqrt(dvalue);
							if (dvalue <= nodeCurrent.getRadius()) {
								nodeCurrent.getNodes().add(nodeTemp.getNum());
								nodeCurrent.getDvalues().put(nodeTemp.getNum(), dvalue);
							}
						}
					}	
				}
			}	
		}
		toAdjServer(nodes,server,mNodes);
	}
	
	/**
	 * 生成基站邻接表
	 */
	private static void toAdjServer(List<Node> nodes, Server server,List<Integer> mNodes) {
		for (Node node : nodes) {
			if(!mNodes.contains(node.getNum())){
				int dvalueServer = (int) (Math.pow((node.getX() - server.getX()), 2)
						+ Math.pow((node.getY() - server.getY()), 2));
				dvalueServer = (int) Math.sqrt(dvalueServer);
				if (dvalueServer <= server.getRadius()) {
					server.getNodes().add(node.getNum());
					server.getDvalues().put(node.getNum(), dvalueServer);
				}
			}
		}
	}
	
	/**
	 * 重建节点网络邻接表
	 */
	public static void rebuild(ArrayList<Node> nodes, Server server){
		clear(nodes, server);
		toAdjNodes(nodes, server);
	}
	
	/**
	 * 重建节点网络邻接表并排除其中的恶意节点
	 * @param num  恶意节点编号
	 */
	public static void rebuild(ArrayList<Node> nodes, Server server,List<Integer> mNodes){
		clear(nodes, server);
		toAdjNodes(nodes, server, mNodes);
	}
	
	/**
	 * 清楚节点网络邻接表
	 * @param nodes 节点列表
	 * @param server 基站对象
	 */
	private static void clear(ArrayList<Node> nodes, Server server){
		for (Node nodeCurrent : nodes) {
			nodeCurrent.setDvalues(new HashMap<Integer, Integer>());
			nodeCurrent.setNodes(new LinkedList<Integer>());
		}
		
		server.setDvalues(new HashMap<Integer, Integer>());
		server.setNodes(new LinkedList<Integer>());
	}
}
