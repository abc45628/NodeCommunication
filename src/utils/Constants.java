package utils;

public class Constants {
  /**
   * 加解密的密钥
   */
  public static final String KEY = "111";

  /**
   * 常见问题
   */
  public static final String PROBLEM = "一、配置节点网络时的参数限制\n" + "    1、节点数量应尽量控制在100以内\n" + "    2、节点半径在150至200之间最佳\n"
      + "    3、电量不能小于功耗\n\n\n" + "二、生成多路径时为何卡顿\n" + "    目前多路径采用递归算法，因此效率低下，有待改进";

  /**
   * 关于
   */
  public static final String ABOUT = "制作：聂立勤，张汇发\n指导老师：章志明";
  /**
   * 协议规则
   */
  public static final String PROTOCOL_RULE = "协议文件的每一行记录一条协议，\n每一条协议的格式是：要发送消息的节点编号、空格、消息内容\n示例：2 message\n注意：2与message之间有一个空格";
  /**
   * 快捷键
   */
  public static final String SHORT_KEY = "还没有O__O ...";
}
