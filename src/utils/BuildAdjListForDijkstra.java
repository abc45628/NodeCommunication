package utils;

import java.util.ArrayList;
import java.util.List;

import bean.NextNode;
import bean.Node;
import bean.Server;

/**
 * 工具类，用于生成nodes和server的邻接表
 * <br>通过传入nodes和server构建该类；
 * <br>通过getNodes()和getServer()方法可以获取更新后的成员；
 * @author-team3
 **/
public class BuildAdjListForDijkstra {
	/**
	 * 生成结点邻接表
	 */
	public static void toAdj_Nodes(ArrayList<Node> nodes,Server server){
		int count = 0;
		for(Node nodeCurrent:nodes){
			boolean isFirstTime = true;
			NextNode tempNode = null;
			NextNode lastAdjNode = null;
			int Dvalue_Server = (int) (Math.pow((nodeCurrent.getX()-server.getX()),2)+
					Math.pow((nodeCurrent.getY()-server.getY()),2));
			Dvalue_Server = (int) Math.sqrt(Dvalue_Server);
			if(Dvalue_Server<=nodeCurrent.getRadius()){
				nodes.get(count).setFirstNode(new NextNode(server.getNum(),Dvalue_Server));
				isFirstTime = false;
				lastAdjNode = nodes.get(count).firstNode;
			}
			for(Node nodeTemp:nodes){
				if(nodeCurrent.getNum()==nodeTemp.getNum()){}else{
					int Dvalue = (int) (Math.pow((nodeCurrent.getX()-nodeTemp.getX()),2)+
							Math.pow((nodeCurrent.getY()-nodeTemp.getY()),2));
					Dvalue = (int) Math.sqrt(Dvalue);
					tempNode = new NextNode(nodeTemp.getNum(),Dvalue);
					if(Dvalue<=nodeCurrent.getRadius()){
						if(isFirstTime){
							nodes.get(count).setFirstNode(tempNode);
							isFirstTime = false;
						}
						else{
							lastAdjNode.nextNode = tempNode;
						}
						lastAdjNode = tempNode;
					}
				}
			}
			count++;
		}
		
		toAdj_Server(nodes,server);
	}
	/**
	 * 生成基站邻接表
	 */
	private static void toAdj_Server(ArrayList<Node> nodes,Server server){
		boolean isFirstTime = true;
		NextNode tempNode = null;
		NextNode lastAdjNode = null;
		for(Node node:nodes){
			int Dvalue_Server = (int) (Math.pow((node.getX()-server.getX()),2)+
					Math.pow((node.getY()-server.getY()),2));
			Dvalue_Server = (int) Math.sqrt(Dvalue_Server);
			tempNode = new NextNode(node.getNum(),Dvalue_Server);
			if(Dvalue_Server<=server.getRadius()){
				if(isFirstTime){
					server.setFirstNode(tempNode);
					isFirstTime=false;
				}
				else{
					lastAdjNode.nextNode = tempNode;
				}
				lastAdjNode = tempNode;
			}
		}
	}
	
	/**
	 * 生成结点邻接表
	 */
	public static void toAdj_Nodes(List<Node> nodes,Server server,List<Integer> mNodes){
		int count = 0;
		for(Node nodeCurrent:nodes){
			if(!mNodes.contains(nodeCurrent.getNum())){
				boolean isFirstTime = true;
				NextNode tempNode = null;
				NextNode lastAdjNode = null;
				int Dvalue_Server = (int) (Math.pow((nodeCurrent.getX()-server.getX()),2)+
						Math.pow((nodeCurrent.getY()-server.getY()),2));
				Dvalue_Server = (int) Math.sqrt(Dvalue_Server);
				if(Dvalue_Server<=nodeCurrent.getRadius()){
					nodes.get(count).setFirstNode(new NextNode(server.getNum(),Dvalue_Server));
					isFirstTime = false;
					lastAdjNode = nodes.get(count).firstNode;
				}
				for(Node nodeTemp:nodes){
					if(!mNodes.contains(nodeTemp.getNum())){
						if(nodeCurrent.getNum()==nodeTemp.getNum()){}else{
							int Dvalue = (int) (Math.pow((nodeCurrent.getX()-nodeTemp.getX()),2)+
									Math.pow((nodeCurrent.getY()-nodeTemp.getY()),2));
							Dvalue = (int) Math.sqrt(Dvalue);
							tempNode = new NextNode(nodeTemp.getNum(),Dvalue);
							if(Dvalue<=nodeCurrent.getRadius()){
								if(isFirstTime){
									nodes.get(count).setFirstNode(tempNode);
									isFirstTime = false;
								}
								else{
									lastAdjNode.nextNode = tempNode;
								}
								lastAdjNode = tempNode;
							}
						}
					}				
				}	
			}
			count++;
		}
		
		toAdj_Server(nodes,server,mNodes);
	}
	/**
	 * 生成基站邻接表
	 */
	private static void toAdj_Server(List<Node> nodes,Server server,List<Integer> mNodes){
		boolean isFirstTime = true;
		NextNode tempNode = null;
		NextNode lastAdjNode = null;
		for(Node node:nodes){
			if(!mNodes.contains(node.getNum())){
				int Dvalue_Server = (int) (Math.pow((node.getX()-server.getX()),2)+
						Math.pow((node.getY()-server.getY()),2));
				Dvalue_Server = (int) Math.sqrt(Dvalue_Server);
				tempNode = new NextNode(node.getNum(),Dvalue_Server);
				if(Dvalue_Server<=server.getRadius()){
					if(isFirstTime){
						server.setFirstNode(tempNode);
						isFirstTime=false;
					}
					else{
						lastAdjNode.nextNode = tempNode;
					}
					lastAdjNode = tempNode;
				}
			}	
		}
	}
	
	/**
	 * 重建节点网络邻接表
	 */
	public static void rebuild(ArrayList<Node> nodes, Server server){
		clear(nodes, server);
		toAdj_Nodes(nodes, server);
	}
	
	/**
	 * 重建节点网络邻接表并排除其中的恶意节点
	 * @param num  恶意节点编号
	 */
	public static void rebuild(ArrayList<Node> nodes, Server server,List<Integer> mNodes){
		clear(nodes, server);
		toAdj_Nodes(nodes, server, mNodes);
	}
	
	/**
	 * 清楚节点网络邻接表
	 * @param nodes 节点列表
	 * @param server 基站对象
	 */
	private static void clear(ArrayList<Node> nodes, Server server){
		for (Node nodeCurrent : nodes) {
			nodeCurrent.setFirstNode(null);
		}
		server.setFirstNode(null);
	}
}
