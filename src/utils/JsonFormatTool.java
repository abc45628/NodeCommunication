package utils;

public class JsonFormatTool {
  /**
   * 一个缩进，2个空格
   */
  private static final String SPACE = "  ";

  /**
   * 换行字符
   */
  private static final String ENTER = "\n";

  /**
   * 记录缩进量
   */
  private static int num = 0;

  private static char[] cs;
  private static char current;

  public static String formatJson(String jsonString) {
    StringBuffer buffer = new StringBuffer();
    cs = jsonString.toCharArray();

    for (int i = 0; i < cs.length; i++) {
      current = cs[i];

      // 如果当前字符是前方括号、前花括号
      if ((current == '[') || (current == '{')) {
        buffer.append(qian(i));
        continue;
      }

      // 如果当前字符是后方括号、后花括号
      if ((current == ']') || (current == '}')) {
        buffer.append(hou(i));
        continue;
      }

      // 如果当前字符是逗号
      if ((current == ',')) {
        buffer.append(douhao());
        continue;
      }

      // 其他情况直接输出当前字符
      buffer.append(current);
    }

    return buffer.toString();
  }

  /**
   * 当前字符是前方括号、前花括号的处理方式
   * 
   * @param i
   *          第i个字符
   */
  private static StringBuffer qian(int i) {
    StringBuffer buffer = new StringBuffer();
    // 如果前面还有字符，并且字符为“：”，增加换行和缩进。
    if ((i - 1 >= 0) && (':' == cs[i - 1])) {
      buffer.append(ENTER);
      buffer.append(addSpace());
    }

    buffer.append(current);

    // 前方括号、前花括号，的后面必须换行。
    buffer.append(ENTER);
    // 每次出现前方括号、前花括号,缩进次数加一,输出换行和新的缩进。
    num++;
    buffer.append(addSpace());
    return buffer;
  }

  /**
   * 当前字符是后方括号、后花括号的处理方式
   * 
   * @param i
   *          第i个字符
   */
  private static StringBuffer hou(int i) {
    StringBuffer buffer = new StringBuffer();
    // 后方括号、后花括号的前面必须换行。
    buffer.append(ENTER);

    // 每出现一次后方括号、后花括号；缩进次数减少一次。
    num--;
    buffer.append(addSpace());

    buffer.append(current);

    // 如果当前字符后面还有字符，并且字符不为“，”，打印：换行。
    if (((i + 1) < cs.length) && (',' != cs[i + 1])) {
      buffer.append(ENTER);
    }
    return buffer;
  }

  /**
   * 当前字符是后方括号、后花括号的处理方式
   */
  private static StringBuffer douhao() {
    // 逗号后面换行，并缩进，不改变缩进次数
    StringBuffer buffer = new StringBuffer();
    buffer.append(current);
    buffer.append(ENTER);
    buffer.append(addSpace());
    return buffer;
  }

  /**
   * 负责返回合适数量的缩进量
   */
  private static String addSpace() {
    StringBuffer buffer = new StringBuffer();
    for (int i = 0; i < num; i++) {
      buffer.append(SPACE);
    }
    return buffer.toString();
  }
}
