package ui;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class MessageLogDialog extends JDialog {

	private static final long serialVersionUID = -1116609520868743557L;

	// 基站消息记录显示框
	private JTextArea jtaMessageLog = new JTextArea();

	// 滚动条，时记录能够滚动查看
	private JScrollPane jspMessageLog;

	public MessageLogDialog(MainWindow parent, boolean model, String messageLog) {
		super(parent, model);

		setTitle("消息记录");
		setBounds(300, 150, 800, 500);
		setResizable(false);
		setLayout(null);

		jspMessageLog = new JScrollPane(jtaMessageLog);
		jspMessageLog.setBounds(10, 10, 775, 455);
		jspMessageLog.setVisible(true);
		jtaMessageLog.setLineWrap(true);
		jtaMessageLog.setWrapStyleWord(true);
		jtaMessageLog.setEditable(false);
		jtaMessageLog.setText(messageLog);

		add(jspMessageLog);
	}

	public MessageLogDialog(MainWindow parent, boolean model, String messageLog, String title) {
		this(parent, model, messageLog);
		setTitle(title);
	}
}
