package ui;

import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import bean.Datas;
import bean.Node;
import bean.Protocol;
import service.CallbackInter;
import service.ProtocolServiceImpl;
import ui.MainWindow.CheckState;
import utils.Constants;
import utils.EncryptUtil;
import utils.FontUtil;

public class LeftPanel extends JPanel implements CheckState {

  private static final long serialVersionUID = -221593292220276585L;
  /**
   * 加密指示
   */
  private boolean encryptFlag = false;
  /**
   * 一次运行的行数
   */
  private JTextField hangshu;
  // 回调接口
  private CallbackInter callback;
  /**
   * 配置信息状态
   */
  private boolean flag = false;
  MainWindow mainWindow;
  /**
   * 记录着发送消息起始节点
   */
  int node = 0;
  /**
   * 记录要发送的消息
   */
  String message;
  /**
   * 协议读取
   */
  private ProtocolServiceImpl psi;
  /**
   * 运行一行协议，记录当前到第几行
   */
  int row_count = 0;
  private boolean isMulitRoad = false;

  /**
   * 运行记录
   */
  private JTextArea runingInfo;

  public LeftPanel(CallbackInter callback) {
    this.callback = callback;
    if (callback instanceof MainWindow) {
      mainWindow = (MainWindow) callback;
    }
    setLayout(null);
    FontUtil.initGlobalFont(new Font("微软雅黑", Font.PLAIN, 13)); // 统一设置字体
    addOpition();
    addRun();
    addPath();
  }

  /*************** 参数部分 START *****************/
  private void addOpition() {

    JLabel label_opition = new JLabel("参数");
    label_opition.setBounds(5, 5, 30, 15);
    add(label_opition);

    Box horizontalBox = Box.createHorizontalBox();
    horizontalBox.setBounds(10, 35, 190, 30);
    horizontalBox.setBackground(new Color(250, 250, 250));
    add(horizontalBox);

    JButton button_opition = new JButton("配置");
    horizontalBox.add(button_opition);

    JButton button_opition_random = new JButton("随机");
    horizontalBox.add(button_opition_random);

    JButton button_opition_reset = new JButton("重置");
    horizontalBox.add(button_opition_reset);

    /////////////// 点击事件///////////////

    button_opition.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (!flag) {
          new SetupDialog((MainWindow) callback, true, callback, LeftPanel.this).setVisible(true);
        } else {
          JOptionPane.showMessageDialog(null, "配置信息已存在，如需重建请先重置！", "Error", JOptionPane.ERROR_MESSAGE);
        }
      }
    });

    button_opition_random.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (!flag) callback.setupDatasRandom(LeftPanel.this);
        else JOptionPane.showMessageDialog(null, "配置信息已存在，如需重建请先重置！", "Error", JOptionPane.ERROR_MESSAGE);
      }
    });

    button_opition_reset.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        if (flag) {
          int state = JOptionPane.showConfirmDialog((MainWindow) callback, "重置将会清空当前所有配置信息，您确定要重置吗？", "重置",
              JOptionPane.YES_NO_OPTION);
          if (state == JOptionPane.YES_OPTION) {
            callback.resetDatas();
            flag = false;
          }
        }
      }
    });
  }

  /*************** 参数部分 END *******************/

  /*************** 运行部分 START *****************/
  private void addRun() {

    JSeparator separator = new JSeparator();
    separator.setBounds(5, 80, 200, 2);
    add(separator);
    JLabel label_1 = new JLabel("运行");
    label_1.setBounds(5, 90, 30, 15);
    add(label_1);

    Box horizontalBox = Box.createHorizontalBox();
    horizontalBox.setBounds(10, 120, 190, 30);
    add(horizontalBox);

    final JCheckBox checkBox_encryption = new JCheckBox("加密");
    checkBox_encryption.setBackground(new Color(250, 250, 250));
    checkBox_encryption.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        encryptFlag = checkBox_encryption.isSelected();
      }
    });
    horizontalBox.add(checkBox_encryption);

    final JCheckBox checkBox_catchAndPause = new JCheckBox("捕获时暂停");
    checkBox_catchAndPause.setBackground(new Color(250, 250, 250));
    checkBox_catchAndPause.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        mainWindow.getDatas().setStop(checkBox_catchAndPause.isSelected());
      }
    });
    horizontalBox.add(checkBox_catchAndPause);

    JLabel label_2 = new JLabel("一次");
    label_2.setBounds(10, 163, 30, 15);
    add(label_2);

    hangshu = new JTextField();
    hangshu.setBounds(39, 160, 66, 21);
    hangshu.setColumns(10);
    add(hangshu);

    JLabel label_3 = new JLabel("行");
    label_3.setBounds(106, 163, 54, 15);
    add(label_3);

    Box horizontalBox_1 = Box.createHorizontalBox();
    horizontalBox_1.setBounds(10, 200, 223, 30);
    add(horizontalBox_1);

    JButton yihang_btn = new JButton("一行");
    horizontalBox_1.add(yihang_btn);

    JButton auto_btn = new JButton("自动");
    horizontalBox_1.add(auto_btn);

    JButton run_btn = new JButton("执行");
    horizontalBox_1.add(run_btn);

    JSeparator separator_1 = new JSeparator();
    separator_1.setBounds(5, 242, 200, 2);
    add(separator_1);

    // ***********按钮事件*****************//
    // 运行一行
    yihang_btn.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (psi != null) {
          List<Protocol> list = psi.getRowList();
          if (list != null) {
            if (row_count < list.size()) {
              Protocol p = list.get(row_count);
              row_count++;
              if (p.getNode() > 0 && (p.getNode() - 1) < mainWindow.getDatas().getNodes().size()) {
                callback.buildPath(p.getNode(), isMulitRoad);
                Integer[] path = mainWindow.getDatas().getdPath();
                send(p.getNode(), 0, p.getMessage(), path);
              }
            }
          }
        } else {
          JOptionPane.showMessageDialog(null, "协议文件没有选择！", "Error", JOptionPane.ERROR_MESSAGE);
        }
      }
    });
    // 运行所有行
    auto_btn.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (psi != null) {
          List<Protocol> list = psi.getRowList();
          if (list != null) {
            for (Protocol p : list) {
              callback.buildPath(p.getNode(), isMulitRoad);
              if (p.getNode() > 0 && (p.getNode() - 1) < mainWindow.getDatas().getNodes().size()) {
                Integer[] path = mainWindow.getDatas().getdPath();
                send(p.getNode(), 0, p.getMessage(), path);
              }
            }
          }
        } else {
          JOptionPane.showMessageDialog(null, "协议文件没有选择！", "Error", JOptionPane.ERROR_MESSAGE);
        }
      }
    });

    // 一次运行若干行
    run_btn.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (psi != null) {
          try {
            int hang = Integer.valueOf(hangshu.getText());
            List<Protocol> list = psi.getRowList();
            if (list != null) {
              for (int i = 0; i < hang && row_count < list.size(); i++) {
                Protocol p = list.get(row_count);
                row_count++;
                if (p.getNode() > 0 && (p.getNode() - 1) < mainWindow.getDatas().getNodes().size()) {
                  callback.buildPath(p.getNode(), isMulitRoad);
                  Integer[] path = mainWindow.getDatas().getdPath();
                  send(p.getNode(), 0, p.getMessage(), path);
                }
              }
            }
          } catch (Exception e2) {
            e2.printStackTrace();
          }
        } else {
          JOptionPane.showMessageDialog(null, "协议文件没有选择！", "Error", JOptionPane.ERROR_MESSAGE);
        }
      }
    });
  }

  /*************** 运行部分 END *******************/

  /*************** 路径部分 START *****************/

  private void addPath() {

    JLabel label_1 = new JLabel("路径");
    label_1.setBounds(5, 252, 54, 15);
    add(label_1);

    final JCheckBox checkBox = new JCheckBox("开启自动规划");
    checkBox.setBounds(10, 282, 103, 23);
    checkBox.setBackground(new Color(250, 250, 250));
    add(checkBox);

    JLabel label_2 = new JLabel("手动选择");
    label_2.setBounds(10, 315, 54, 15);
    add(label_2);

    final JRadioButton radioButton = new JRadioButton("路程最短");
    radioButton.setBounds(20, 340, 121, 23);
    radioButton.setBackground(new Color(250, 250, 250));
    add(radioButton);

    final JRadioButton radioButton_1 = new JRadioButton("资源最多");
    radioButton_1.setBounds(20, 370, 121, 23);
    radioButton_1.setBackground(new Color(250, 250, 250));
    add(radioButton_1);

    final JButton preButton = new JButton("上一条");
    preButton.setBounds(10, 400, 85, 30);
    add(preButton);
    final JButton nextButton = new JButton("下一条");
    nextButton.setBounds(105, 400, 85, 30);
    add(nextButton);

    radioButton.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        Object obj = e.getItem();
        if (obj.equals(radioButton)) {
          if (radioButton.isSelected()) {
            radioButton_1.setSelected(false);
            Datas.setPathType(Datas.THE_SHORTEST);
            callback.getPath();
          }
        }
      }
    });

    radioButton_1.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        Object obj = e.getItem();
        if (obj.equals(radioButton_1)) {
          if (radioButton_1.isSelected()) {
            radioButton.setSelected(false);
            Datas.setPathType(Datas.THE_MOST_POWERFUL);
            callback.getPath();
          }
        }
      }
    });

    nextButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        radioButton.setSelected(false);
        radioButton_1.setSelected(false);
        Datas.setPathType(Datas.NEXT);
        callback.getPath();
      }
    });

    preButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        radioButton.setSelected(false);
        radioButton_1.setSelected(false);
        Datas.setPathType(Datas.PRE);
        callback.getPath();
      }
    });

    // 开启自动规划的点击事件,默认是选择路径最短
    checkBox.addItemListener(new ItemListener() {

      public void itemStateChanged(ItemEvent e) {
        Object obj = e.getItem();
        if (obj.equals(checkBox)) {
          if (checkBox.isSelected()) {
            radioButton.setSelected(false);
            radioButton.setEnabled(false);
            radioButton_1.setSelected(false);
            radioButton_1.setEnabled(false);
            preButton.setEnabled(false);
            nextButton.setEnabled(false);
            Datas.setPathType(Datas.AUTO);
            callback.getPath();
          } else {
            radioButton.setEnabled(true);
            radioButton_1.setEnabled(true);
            preButton.setEnabled(true);
            nextButton.setEnabled(true);
            radioButton.setSelected(true);
            radioButton_1.setSelected(false);
          }
        }
      }
    });

    JLabel label_3 = new JLabel("从节点");
    label_3.setBounds(5, 440, 40, 15);
    add(label_3);

    final JTextField node_from_to = new JTextField();
    node_from_to.setBounds(45, 437, 30, 21);
    add(node_from_to);

    JLabel label_4 = new JLabel("到基站的通讯路径");
    label_4.setBounds(75, 440, 105, 15);
    add(label_4);

    final JRadioButton singleRoad = new JRadioButton("单路径");
    singleRoad.setBounds(5, 465, 100, 15);
    singleRoad.setBackground(new Color(250, 250, 250));
    singleRoad.setSelected(true);
    singleRoad.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        isMulitRoad = !singleRoad.isSelected();
      }
    });
    add(singleRoad);

    final JRadioButton muiltRoad = new JRadioButton("多路径");
    muiltRoad.setBounds(5, 480, 100, 15);
    muiltRoad.setBackground(new Color(250, 250, 250));
    muiltRoad.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        isMulitRoad = muiltRoad.isSelected();
      }
    });
    add(muiltRoad);

    ButtonGroup bg = new ButtonGroup();
    bg.add(singleRoad);
    bg.add(muiltRoad);

    final JButton buildPath = new JButton("生成");
    buildPath.setBounds(106, 465, 84, 23);
    buildPath.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          node = Integer.valueOf(node_from_to.getText());
          if (node > 0 && (node - 1) < mainWindow.getDatas().getNodes().size()) {
            ArrayList<Node> nList = mainWindow.getDatas().getNodes();
            for (Node n : nList) {
              if (node == n.getNum()) {
                if (n.isMaliciousNode()) {
                  JOptionPane.showMessageDialog(null, "无法建立恶意节点" + node + "到基站的链接", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                  callback.buildPath(node, isMulitRoad);
                }
                break;
              }
            }
          }
        } catch (NumberFormatException e2) {
          e2.printStackTrace();
        }
      }
    });
    add(buildPath);

    JLabel label_5 = new JLabel("向节点发送消息");
    label_5.setBounds(5, 498, 100, 21);
    add(label_5);

    final JTextField msn = new JTextField();
    msn.setBounds(10, 529, 85, 21);
    add(msn);
    msn.setColumns(10);

    JButton send = new JButton("发送");
    send.setBounds(106, 528, 84, 23);
    send.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        buildPath.doClick();
        if (node > 0 && (node - 1) < mainWindow.getDatas().getNodes().size()) {
          Integer[] path = mainWindow.getDatas().getdPath();
          if (path != null) {
            message = msn.getText();
            send(node, 0, message, path);
          }
        }
      }
    });
    add(send);

    JButton readProtocol = new JButton("读取脚本");
    readProtocol.setBounds(5, 576, 90, 23);
    readProtocol.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        MainWindow mainWindow = null;
        if (callback instanceof MainWindow) {
          mainWindow = (MainWindow) callback;
        }
        if (mainWindow != null) {
          File file = null;
          FileDialog fd = new FileDialog(mainWindow, "请选择脚本文件");
          fd.setVisible(true);
          if (fd.getFile() != null) {
            file = new File(fd.getDirectory(), fd.getFile());
          }
          if (file != null) {
            try {
              psi = new ProtocolServiceImpl(file);
              List<Protocol> protocols = psi.read();
              row_count = 0;
              mainWindow.setTitle("节点通讯 - " + file.getPath() + " - 协议文件");
              //runingInfo.append(psi.toString() + "\n");
              System.out.println(protocols);
            } catch (FileNotFoundException e1) {
              e1.printStackTrace();
            }
          }
        }
      }
    });
    add(readProtocol);
  }

  // 修改配置信息状态
  public void setFlag(boolean flag) {
    this.flag = flag;
  }

  // 发送消息
  private void send(int souceNode, int targetNode, String message, Integer[] path) {
    if (encryptFlag) {
      callback.sendMessage(souceNode, targetNode, message, EncryptUtil.encrypt(message, Constants.KEY), path);
    } else {
      callback.sendMessage(souceNode, targetNode, message, path);
    }
  }

  // ************grt()set()*******************//
  public ProtocolServiceImpl getPsi() {
    return psi;
  }
}
