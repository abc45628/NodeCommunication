package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import service.CallbackInter;
import bean.Datas;
import bean.Node;

public class NumInputDialog extends JDialog{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6147483476763553954L;

	/**
	 * 查看节点信息
	 */
	public static final int NODE = 1;
	
	/**
	 * 查看恶意节点信息
	 */
	public static final int MALICIOUS_NODE = 2;
	
	/**
	 * 窗口类型
	 */
	public int type = NODE;
	
	/**
	 * 编号输入窗口提示信息
	 */
	private JLabel jlNum = new JLabel();
	
	/**
	 * 编号输入窗口
	 */
	private JTextField jtfNum = new JTextField();
	
	/**
	 * 确认按钮
	 */
	private JButton enter = new JButton("确定");
	
	/**
	 * 数据存储库
	 */
	private Datas datas = null;
	
	/**
	 * 回调接口
	 */
	private CallbackInter callback = null;
	
	public NumInputDialog(MainWindow parent,boolean model,int type,Datas datas,CallbackInter callback) {
		//调用父类方法使主窗口挂起
		super(parent,model);
		
		this.type = type;
		this.datas = datas;
		this.callback = callback;
		
		setTitle("输入编号以查询信息");
		setBounds(550, 300, 250, 100);
		setSize(250, 150);
		
		
		setResizable(false);
		setLayout(null);
		
		initUI();

	}

	private void initUI() {
		
		jlNum.setBounds(15, 30, 100, 50);
		jlNum.setHorizontalAlignment(JLabel.CENTER);
		
		jtfNum.setBounds(120, 30, 30, 35);
		
		enter.setBounds(160, 30, 65, 34);
		
		add(jlNum);
		add(jtfNum);
		add(enter);
		
		String text = "";
		
		if (type == NODE)
			text = "节  点  编  号  ：";
		else 
			text = "恶意节点编号：";
		
		jlNum.setText(text);
		
		enter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String text = jtfNum.getText().toString().trim();
				if(text == null || text.equals("")){}
				else{
					try{
						//获取编号
						int num = Integer.parseInt(text);
						
						if(num == 0){
							JOptionPane.showMessageDialog(null, "不存在编号为0的节点！", "Error",
									JOptionPane.ERROR_MESSAGE);
						} else if(num > datas.getNodes().size()){
							JOptionPane.showMessageDialog(null, "不存在编号为" + num + "的节点！", "Error",
									JOptionPane.ERROR_MESSAGE);
						} else{
							Node node = datas.getNodes().get(num - 1);
							if(type == NODE){
								if(!node.isMaliciousNode()){
									// 弹出修改窗口
									new UpdateNodeInfoDialog((MainWindow) callback, true, node,
											num - 1, callback).setVisible(true);
								}else{
									JOptionPane.showMessageDialog(null, "该节点为恶意节点！", "Error",
											JOptionPane.ERROR_MESSAGE);
								}
							}else if(type == MALICIOUS_NODE){
								if(node.isMaliciousNode()){
									// 弹出修改窗口
									new UpdateMaliciousNodeInfoDialog((MainWindow) callback, true, node,
											num - 1, callback).setVisible(true);
								}else{
									JOptionPane.showMessageDialog(null, "该节点不是恶意节点！", "Error",
											JOptionPane.ERROR_MESSAGE);
								}
							}
						}		
						
					}catch(NumberFormatException e){
						JOptionPane.showMessageDialog(null, "请输入正确的编号！", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
				
			}
		});
		
	}
}
