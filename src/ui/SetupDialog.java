package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import service.CallbackInter;
import ui.MainWindow.CheckState;

/**
 * 配置信息录入窗口
 * 
 * @author 聂立勤
 *
 */
public class SetupDialog extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5395182171912489531L;

	private JTextField jNodeNum = new JTextField(3); // 节点个数输入框
	private JTextField jPowerConsumption = new JTextField(5); // 节点功耗输入框
	private JTextField jPower = new JTextField(5); // 节点电量输入框
	private JTextField jRadius = new JTextField(5); // 节点半径输入框

	private JLabel title = new JLabel("配  置  信  息", JLabel.CENTER); // 提示信息

	private JLabel num = new JLabel("数量:");
	private JLabel power = new JLabel("电量:");
	private JLabel powerConsumption = new JLabel("功耗:");
	private JLabel radius = new JLabel("半径:");

	private JButton enter = new JButton("确定");
	private JButton reset = new JButton("重填");

	/**
	 * 回调接口
	 */
	private CallbackInter callback;
	
	/**
	 * 回调接口，用于修改配置信息状态
	 */
	private CheckState checkState;

	/**
	 * SetupWindow的构造函数
	 * @param parent 主窗口引用
	 * @param model 弹窗模式（true为对话框型弹出，主窗口将挂起）
	 * @param callback 回调接口
	 */
	public SetupDialog(MainWindow parent,boolean model,CallbackInter callback,CheckState checkState) {
		//调用父类方法使主窗口挂起
		super(parent,model);
		this.callback = callback;
		this.checkState = checkState;
		

		setTitle("配置节点信息");
		setBounds(500, 200, 400, 400);
		setSize(400, 400);
		
		
		setResizable(false);
		setLayout(null);
		
		initUI();

	}

	/**
	 * 初始化UI
	 */
	private void initUI() {

		title.setBounds(150, 15, 100, 30);

		num.setBounds(50, 70, 45, 30);
		jNodeNum.setBounds(95, 70, 80, 30);

		power.setBounds(225, 70, 45, 30);
		jPower.setBounds(270, 70, 80, 30);

		powerConsumption.setBounds(50, 170, 45, 30);
		jPowerConsumption.setBounds(95, 170, 80, 30);

		radius.setBounds(225, 170, 45, 30);
		jRadius.setBounds(270, 170, 80, 30);

		enter.setBounds(95, 270, 80, 50);
		reset.setBounds(270, 270, 80, 50);

		add(title);
		add(num);
		add(jNodeNum);
		add(power);
		add(jPower);
		add(powerConsumption);
		add(jPowerConsumption);
		add(radius);
		add(jRadius);
		add(enter);
		add(reset);

		// 重置的点击事件
		reset.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				jNodeNum.setText("");
				jPower.setText("");
				jRadius.setText("");
				jPowerConsumption.setText("");
			}
		});

		// 确定输入的信息
		enter.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				/**
				 * 异常类型，用于提示：0为节点数量异常，1为电量异常，2为功耗大于电量异常，3为半径异常
				 */
				int exceptionType = 0;

				//获取数据
				String numStr = jNodeNum.getText().trim().toString();
				String powerStr = jPower.getText().trim().toString();
				String radiusStr = jRadius.getText().trim().toString();
				String powerConsumptionStr = jPowerConsumption.getText().trim()
						.toString();

				// 数据完整性校验
				if (numStr.length() == 0 || powerStr.length() == 0
						|| radiusStr.length() == 0
						|| powerConsumptionStr.length() == 0) {

					JOptionPane.showMessageDialog(null, "请输入完整配置信息！", "Error",
							JOptionPane.ERROR_MESSAGE);

				} else {
					try {

						int num = Integer.valueOf(numStr);
						double power = Double.valueOf(powerStr);
						double radius = Double.valueOf(radiusStr);
						double powerConsumption = Double
								.valueOf(powerConsumptionStr);
						if (num <= 0) {
							exceptionType = 0;
							throw new Exception();
						} else if (power <= 0) {
							exceptionType = 1;
							throw new Exception();
						} else if (powerConsumption >= power) {
							exceptionType = 2;
							throw new Exception();
						} else if (radius <= 0 ) {
							exceptionType = 3;
							throw new Exception();
						}

						// 通知主函数初始化节点网络并将其绘制
						callback.setupDatas(num, powerConsumption, power,
								radius);
						checkState.setFlag(true);
						
						// 销毁
						dispose();

					} catch (NumberFormatException nf) {
						JOptionPane.showMessageDialog(null, "配置信息类型有误！", "Error",
								JOptionPane.ERROR_MESSAGE);
					} catch (Exception ex) {
						switch (exceptionType) {
						case 0:
							JOptionPane.showMessageDialog(null, "节点数量不能小于1！", "Error",
									JOptionPane.ERROR_MESSAGE);
							break;
						case 1:
							JOptionPane.showMessageDialog(null, "节点电量不能小于1！", "Error",
									JOptionPane.ERROR_MESSAGE);
							break;
						case 2:
							JOptionPane.showMessageDialog(null, "节点功耗不能大于其电量！", "Error",
									JOptionPane.ERROR_MESSAGE);
							break;
						case 3:
							JOptionPane.showMessageDialog(null, "节点广播半径不能小于1（推荐半径在100以上）！", "Error",
									JOptionPane.ERROR_MESSAGE);
							break;
						}
					}
					
				}
			}
		});

	}
}
