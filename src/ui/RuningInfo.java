package ui;

import java.awt.BorderLayout;
import java.awt.Rectangle;

import javax.swing.JDialog;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Font;

public class RuningInfo extends JDialog {
  private static final long serialVersionUID = 1L;
  private JTextArea textArea;

  public RuningInfo() {
    setBounds(new Rectangle(0, 0, 800, 250));
    setVisible(true);
    setAlwaysOnTop(true);
    getContentPane().setLayout(new BorderLayout());
    textArea = new JTextArea();
    textArea.setEditable(false);
    textArea.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 17));
    JScrollPane scrollPane = new JScrollPane(textArea);
    getContentPane().add(scrollPane);
  }

  public JTextArea getTextArea() {
    return textArea;
  }

  public void setTextArea(JTextArea textArea) {
    this.textArea = textArea;
  }
}
