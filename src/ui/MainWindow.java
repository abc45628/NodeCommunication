package ui;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import bean.Datas;
import bean.Node;
import bean.Server;
import service.CallbackInter;
import service.CommunicateServiceImpl;
import service.FileServiceImpl;
import service.PathServiceImpl;
import utils.BuildAdjList;
import utils.BuildAdjListForDijkstra;
import utils.Constants;
import utils.FontUtil;
import utils.JsonFormatTool;

public class MainWindow extends JFrame implements CallbackInter {

  private static final long serialVersionUID = 6841497333789115515L;

  public interface CheckState {
    /**
     * 修改配置信息生成状态
     * 
     * @param flag
     *          状态
     */
    public void setFlag(boolean flag);
  }

  /**
   * 顶部菜单栏
   */
  private JMenuBar menuBar = new JMenuBar();

  /**
   * 左侧菜单栏
   */
  private LeftPanel leftPanel = new LeftPanel(this);

  /**
   * 右侧绘板
   */
  private PaintPanel rightPanel = new PaintPanel(this);

  /**
   * 文件选择框
   */
  private FileDialog fileDialog;

  /**
   * 数据存储器
   */
  private Datas datas;

  /**
   * 路径管理器
   */
  private PathServiceImpl pathManager;

  /**
   * 信息管理器
   */
  private CommunicateServiceImpl communicateManager;

  /**
   * 屏幕参数
   */
  Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

  public MainWindow() {
    setTitle("节点通讯");
    setVisible(true);
    setLayout(null);
    // 最小分辨率
    setMinimumSize(new Dimension(680, 400));
    // 设置为最大化
    setExtendedState(MAXIMIZED_BOTH);
    URL url = this.getClass().getResource("icon_image.png");
    Image img = Toolkit.getDefaultToolkit().getImage(url);
    setIconImage(img);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // 设置左右侧模块的大小及左侧菜单栏的背景颜色
    leftPanel.setBackground(new Color(250, 250, 250));
    leftPanel.setBounds(10, 10, 210, screenSize.height - 110);
    rightPanel.setBounds(230, 10, screenSize.width - 242, screenSize.height - 110);

    // 统一设置字体
    FontUtil.initGlobalFont(new Font("微软雅黑", Font.PLAIN, 13));

    // 创建凸起边框
    Border border = BorderFactory.createCompoundBorder(null, BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
    rightPanel.setBorder(border);
    leftPanel.setBorder(border);

    // 设置标题边框的标题对齐方式和颜色
    TitledBorder tb = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.white), "显示区域");
    tb.setTitleJustification(TitledBorder.CENTER);
    tb.setTitleColor(Color.yellow);

    setJMenuBar(menuBar);
    addFileJMenu();
    addEditJMenu();
    addViewJMenu();
    addHelpJMenu();
    add(leftPanel);
    add(rightPanel);
  }

  /**
   * 添加文件菜单
   */
  private void addFileJMenu() {
    JMenu file = new JMenu("文件");
    menuBar.add(file);

    JMenuItem recentlyOpened = new JMenuItem("最近打开");
    file.add(recentlyOpened);

    JMenuItem openFile = new JMenuItem("打开");
    openFile.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        fileDialog = new FileDialog(mainWindow, "打开", FileDialog.LOAD);
        fileDialog.setVisible(true);
        if (fileDialog.getFile() != null) {
          try {
            Reader in = new FileReader(fileDialog.getDirectory() + fileDialog.getFile());
            BufferedReader br = new BufferedReader(in);
            StringBuffer sb = new StringBuffer();
            String line = null;
            while ((line = br.readLine()) != null) {
              sb.append(line);
            }
            br.close();
            resetDatas();

            FileServiceImpl fsi = new FileServiceImpl();
            datas = fsi.openDatas(sb.toString());

            restoreDatas(leftPanel);
          } catch (IOException e1) {
            e1.printStackTrace();
          }
        }
      }
    });
    file.add(openFile);

    JMenuItem save = new JMenuItem("保存");
    file.add(save);
    save.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        FileServiceImpl fsi = new FileServiceImpl();
        String json = JsonFormatTool.formatJson(fsi.saveDatas(datas));
        fileDialog = new FileDialog(mainWindow, "保存", FileDialog.SAVE);
        fileDialog.setVisible(true);
        if (fileDialog.getFile() != null) {
          try {
            FileOutputStream fos = new FileOutputStream(fileDialog.getDirectory() + fileDialog.getFile());
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            bos.write(json.getBytes());
            bos.flush();
            bos.close();
          } catch (IOException e1) {
            e1.printStackTrace();
          }
        }
      }
    });

    JMenuItem saveAsPicture = new JMenuItem("保存图片");
    saveAsPicture.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        int biaoti = mainWindow.getInsets().top - 8;// 标题栏高度,-8是因为win10有边框阴影
        int menuHeight = menuBar.getHeight();// 菜单栏高度
        int right_y = rightPanel.getY();// 右面板离菜单栏高度
        int y = biaoti + menuHeight + right_y;
        try {
          BufferedImage bi = new Robot()
              .createScreenCapture(new Rectangle(rightPanel.getX(), y, rightPanel.getWidth(), rightPanel.getHeight()));
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH_mm_ss");
          File file = new File("d:\\" + sdf.format(System.currentTimeMillis()) + ".jpg");
          ImageIO.write(bi, "jpg", file);
        } catch (IOException | AWTException e1) {
          e1.printStackTrace();
        }
      }
    });
    file.add(saveAsPicture);

    JMenuItem exit = new JMenuItem("退出");
    exit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        System.out.println("已点击退出");
        System.exit(0); // 关闭程序，结束所有进程
      }
    });
    file.add(exit);
  }

  /**
   * 添加编辑菜单
   */
  private void addEditJMenu() {
    JMenu editMenu = new JMenu("编辑");
    menuBar.add(editMenu);

    JMenuItem goback = new JMenuItem("撤销");
    editMenu.add(goback);

    JMenuItem menuItem_17 = new JMenuItem("前进");
    editMenu.add(menuItem_17);

    JMenuItem grid = new JMenuItem("网格");
    grid.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (datas != null) {
          if (datas.isShowTable()) { // 如果已经许可绘制
            // 网格许可
            datas.setShowTable(false);
          } else { // 如果尚未许可绘制
            // 网格许可
            datas.setShowTable(true);
          }
          // 更新画板中的数据
          rightPanel.setDatas(datas);

          // 更新绘板
          updatePaintPanel();
        } else {
          JOptionPane.showMessageDialog(null, "节点网络尚未建立！", "Error", JOptionPane.ERROR_MESSAGE);
        }

      }
    });
    editMenu.add(grid);

    JMenuItem font = new JMenuItem("字体");
    editMenu.add(font);

  }

  /**
   * 添加查看菜单
   */
  private void addViewJMenu() {
    JMenu viewMenu = new JMenu("查看");
    menuBar.add(viewMenu);

    JMenuItem parameters = new JMenuItem("参数信息");
    parameters.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (datas != null) {
          new SetupDialogForRead(MainWindow.this, true, datas).setVisible(true);
        } else {
          JOptionPane.showMessageDialog(null, "节点网络尚未建立！", "Error", JOptionPane.ERROR_MESSAGE);
        }
      }
    });
    viewMenu.add(parameters);

    JMenuItem baseStation = new JMenuItem("基站信息");
    baseStation.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        if (datas != null) {
          // 弹出基站信息修改窗口
          new UpdateServerInfoDialog(MainWindow.this, true, datas.getServer(), MainWindow.this).setVisible(true);
        } else {
          JOptionPane.showMessageDialog(null, "节点网络尚未建立！", "Error", JOptionPane.ERROR_MESSAGE);
        }

      }
    });
    viewMenu.add(baseStation);

    JMenuItem node = new JMenuItem("节点信息");
    node.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        if (datas != null) {
          new NumInputDialog(MainWindow.this, true, NumInputDialog.NODE, datas, MainWindow.this).setVisible(true);
        } else {
          JOptionPane.showMessageDialog(null, "节点网络尚未建立！", "Error", JOptionPane.ERROR_MESSAGE);
        }

      }
    });
    viewMenu.add(node);

    JMenuItem protocol = new JMenuItem("协议内容");
    protocol.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (leftPanel.getPsi() != null) {
          (new MessageLogDialog(mainWindow, true, leftPanel.getPsi().toString(), "协议内容")).setVisible(true);
        } else {
          JOptionPane.showMessageDialog(null, "协议文件没有读取", "Error", JOptionPane.ERROR_MESSAGE);
        }
      }
    });
    viewMenu.add(protocol);

    JMenuItem maliciousNode = new JMenuItem("恶意节点信息");
    maliciousNode.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        if (datas != null) {
          new NumInputDialog(MainWindow.this, true, NumInputDialog.MALICIOUS_NODE, datas, MainWindow.this)
              .setVisible(true);
        } else {
          JOptionPane.showMessageDialog(null, "节点网络尚未建立！", "Error", JOptionPane.ERROR_MESSAGE);
        }

      }
    });
    viewMenu.add(maliciousNode);
  }

  /**
   * 添加帮助菜单
   */
  private void addHelpJMenu() {
    JMenu help = new JMenu("帮助");
    menuBar.add(help);

    JMenuItem shortcutKeys = new JMenuItem("快捷键");
    help.add(shortcutKeys);

    JMenuItem protocolSpecification = new JMenuItem("协议规范");
    help.add(protocolSpecification);

    JMenuItem faq = new JMenuItem("常见问题");
    faq.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        new MessageLogDialog(mainWindow, true, Constants.PROBLEM, "常见问题").setVisible(true);

      }
    });
    help.add(faq);

    JMenuItem about = new JMenuItem("关于");
    about.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        new MessageLogDialog(mainWindow, true, Constants.ABOUT, "关于").setVisible(true);

      }
    });
    help.add(about);
  }

  /*----------------------------实现接口中的方法------------------------------*/

  // 初始化数据
  public void setupDatas(int num, double powerConsumption, double power, double radius) {
    // 如果配置信息不存在
    if (datas == null) {
      datas = new Datas(num, power, powerConsumption, radius, screenSize.width - 242, screenSize.height - 110, this);
      // 更新画板中的数据并重绘
      rightPanel.setDatas(datas);
      updatePaintPanel();

      pathManager = new PathServiceImpl(datas);
      communicateManager = new CommunicateServiceImpl(datas);

    }
  }

  // 重置数据
  public void resetDatas() {
    datas = null;
    pathManager = null;
    communicateManager = null;
    // 更新画板中的数据并重绘
    rightPanel.setDatas(datas);
    // 取消绘制许可
    rightPanel.setState(false);
    // 清空画板
    rightPanel.removeAll();
    updatePaintPanel();
  }

  /**
   * 读取文件后还原成上一次的使用状态
   */
  public void restoreDatas(CheckState checkState) {
    if (datas != null) {
      rightPanel.setDatas(datas);
      updatePaintPanel();
      checkState.setFlag(true);

      pathManager = new PathServiceImpl(datas);
      communicateManager = new CommunicateServiceImpl(datas);
    }
  }

  // 采用随机数据初始化数据
  public void setupDatasRandom(CheckState checkState) {

    // 如果配置信息不存在
    if (datas == null) {
      Random random = new Random();
      int num = random.nextInt(50) + 50;
      double power = 200;
      double powerConsumption = 1;
      double radius = (int) (random.nextDouble() * 100) + 150;
      datas = new Datas(num, power, powerConsumption, radius, screenSize.width - 242, screenSize.height - 110, this);

      // 更新画板中的数据并重绘
      rightPanel.setDatas(datas);
      updatePaintPanel();
      checkState.setFlag(true);

      pathManager = new PathServiceImpl(datas);
      communicateManager = new CommunicateServiceImpl(datas);

    }
  }

  // 更新单个正常节点数据
  public void updateNodeInfo(Node node, int num) {
    datas.getNodes().set(num, node);
    BuildAdjList.rebuild(datas.getNodes(), datas.getServer(), datas.getmNodes());
    BuildAdjListForDijkstra.rebuild(datas.getNodes(), datas.getServer(), datas.getmNodes());
    datas.setdPath(null);

    rightPanel.setDatas(datas);
    updatePaintPanel();

  }

  // 更新基站数据
  public void updateServerInfo(Server server) {
    datas.setServer(server);
    BuildAdjList.rebuild(datas.getNodes(), datas.getServer(), datas.getmNodes());
    BuildAdjListForDijkstra.rebuild(datas.getNodes(), datas.getServer(), datas.getmNodes());
    datas.setdPath(null);

    rightPanel.setDatas(datas);
    updatePaintPanel();
  }

  /**
   * 更新绘板
   */
  public void updatePaintPanel() {
    // 重绘
    rightPanel.repaint();
    // 更新UI
    rightPanel.updateUI();
    // 组件生效
    rightPanel.revalidate();
  }

  // 生成路径表
  @SuppressWarnings("static-access")
  public void buildPath(int num, boolean type) {
    Integer[] path = null;
    if (type) {
      // 生成路径表
      path = pathManager.getPaths(num, 0);
    } else {
      datas.setPathType(Datas.ONLYONE);
      path = pathManager.getDijkstraPath(num, 0);
    }

    if (path != null) {
      datas.setdPath(path);
      // 更新画板中的数据
      rightPanel.setDatas(datas);
      // 更新绘板
      updatePaintPanel();
    } else {
      JOptionPane.showMessageDialog(null, "不存在该节点到基站的路径！", "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  // 获取路径
  @SuppressWarnings("static-access")
  public void getPath() {
    if (pathManager != null) {
      // 当路径模式为自动规划或最短路径时，获取最短路径
      if (datas.getPathType() == datas.AUTO || datas.getPathType() == datas.THE_SHORTEST) {

        datas.setdPath(pathManager.getTheShortestPath());
        // 当路径选择模式为电量最足时，获取电量最足的路径
      } else if (datas.getPathType() == datas.THE_MOST_POWERFUL) {

        datas.setdPath(pathManager.getTheMostPowerfulPath());
        // 当路径选择模式为上一条路径时，选择上一条路径，并清空模式
      } else if (datas.getPathType() == datas.PRE) {

        datas.setdPath(pathManager.getPrePath());
        datas.setPathType(0);
        // 当路径选择模式为下一条路径时，选择下一条路径，并清空模式
      } else if (datas.getPathType() == datas.NEXT) {
        datas.setdPath(pathManager.getNextPath());
        datas.setPathType(0);
      }

      if (datas.getPathType() != datas.ONLYONE) {
        // 更新画板中的数据
        rightPanel.setDatas(datas);
        // 更新绘板
        updatePaintPanel();
      }
    }
  }

  // 发送消息
  public void sendMessage(int souceNode, int targetNode, String message, Integer[] path) {
    communicateManager.sendMessage(souceNode, targetNode, message, path);
  }

  // 发送消息
  public void sendMessage(int souceNode, int targetNode, String message, byte[] encryptMessage, Integer[] path) {
    communicateManager.sendMessage(souceNode, targetNode, message, encryptMessage, path);
  }

  // 屏蔽节点网络中的恶意节点
  public void deleteMaliciousNode(List<Integer> mNodes) {
    BuildAdjList.rebuild(datas.getNodes(), datas.getServer(), mNodes);
    BuildAdjListForDijkstra.rebuild(datas.getNodes(), datas.getServer(), mNodes);
    datas.setmNodes(mNodes);
    datas.setdPath(null);
    // 更新画板中的数据
    rightPanel.setDatas(datas);

    // 更新绘板
    updatePaintPanel();

    if (datas.isStop()) {
      JOptionPane.showMessageDialog(null, "已捕获到一个新的恶意节点" + mNodes.get(mNodes.size() - 1) + " ！", "PLAIN",
          JOptionPane.PLAIN_MESSAGE);
    }
  }

  /*----------------------------程序启动模块------------------------------*/

  private static MainWindow mainWindow;

  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          mainWindow = new MainWindow();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /*---------------------------get(),set()--------------------------------*/
  public Datas getDatas() {
    return datas;
  }

}
