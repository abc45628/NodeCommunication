package ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;

import service.CallbackInter;
import utils.FontUtil;
import bean.Datas;
import bean.Node;

/**
 * 绘板，用于绘制网格，节点网络，路径等可视化信息
 * @author 聂立勤
 *
 */
public class PaintPanel extends JPanel implements MouseListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9166492185885372383L;

	/**
	 * 数据存放处
	 */
	private Datas datas = null;
	/**
	 * 绘制背景为空
	 */
	private BufferedImage image = null;
	/**
	 * 绘制许可，false为不允许绘图
	 */
	private boolean state = false;
	/**
	 * 节点容器，用于显示编号和发起点击事件
	 */
	private JLabel lbNodePosition;
	/**
	 * 基站图片
	 */
	private BufferedImage background;
	/**
	 * 计时器,用于处理双击
	 */
	private long firstClick = 0;
	/**
	 * 回调接口
	 */
	private CallbackInter callback;
	
	/**
	 * 屏幕参数
	 */
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

	public PaintPanel(CallbackInter callback) {
		// 处理画板内字体
		FontUtil.initGlobalFont(new Font("微软雅黑", Font.PLAIN, 12));
		this.callback = callback;
	}

	/**
	 * 数据更新
	 * 
	 * @param datas
	 *            数据存放器
	 */
	public void setDatas(Datas datas) {
		this.datas = datas;
		//datas.setdPath(new int[] { 0, 12, 15, 17, 2, 6 ,0}); //路径绘制测试数据
		state = true;
	}

	/**
	 * 核心函数，用于绘制图形，增加控件
	 */
	public void paintComponent(Graphics g) {
		//初始化画笔
		Graphics2D g2d = (Graphics2D) g.create();
		//清空画板
		g2d.clearRect(0, 0, this.getWidth(), this.getHeight());
		//绘制一块空背景
		g2d.drawImage(image, 0, 0, null);
		//如果允许绘制
		if (state) {
			// 开始绘制
			// 首先移除所有画板上的图案
			this.removeAll();
			/**********************       绘制网格                *********************/
			//如果需要绘制
			if(datas.isShowTable()){
				//绘制网格
				paintTable(g2d);
			}
			
			/********************** 遍历所有节点并绘制成网络 *********************/
			
			for (Node node : datas.getNodes()) {
				// 当为可靠节点时进行绘制
				if (node.getNum() != 0) {
					if(datas.getmNodes()!= null && datas.getmNodes().contains(node.getNum())){
						// 恶意节点背景采用红色背景
						g2d.setColor(new Color(240,120,3));
					}else{
						// 节点背景采用淡蓝色
						g2d.setColor(new Color(51, 204, 204));
					}
					// 大小为16x16，前俩个参数为偏移量，用于使数字能够居中显示
					g2d.fillOval(node.getX() - 10, node.getY() - 10, 20, 20);
					//设置画笔抗锯齿
					g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
					// 显示编号
					lbNodePosition = new JLabel("" + node.getNum(),
							JLabel.CENTER);
					// 设置编号颜色
					lbNodePosition.setForeground(Color.black);
					//设置可点击范围
					lbNodePosition.setBounds(node.getX() - 14,
							node.getY() - 14, 28, 28);
					//添加点击事件
					lbNodePosition.addMouseListener(this);
					//加至Panel
					add(lbNodePosition);
				}
			}
			/********************** 开始绘制基站 *********************/
			try {
				// 读取基站图片
				background = ImageIO.read(getClass().getResource(
						"BaseStation.jpg"));
				// 绘制基站
				g2d.drawImage(background, datas.getServer().getX(), datas
						.getServer().getY(), null);
				lbNodePosition = new JLabel("" + datas.getServer().getNum(),
						JLabel.CENTER);
				lbNodePosition.setFont(new Font("微软雅黑", Font.PLAIN, 0));
				lbNodePosition.setBounds(datas.getServer().getX(), datas
						.getServer().getY(), 50, 50);
				lbNodePosition.addMouseListener(this);
				add(lbNodePosition);
			} catch (IOException e) {
				e.printStackTrace();
			}

			/********************** 开始绘制路径 *********************/
			if (datas.getdPath() != null) {
				//绘制路径
				drawLine(g2d, datas.getdPath());
			}
			// 销毁画笔
			g2d.dispose();
		}
	}
	
	/**
	 * 绘制网格
	 * @param g2d 画笔
	 */
	private void paintTable(Graphics2D g2d){
		//设置网格颜色
		g2d.setColor(new Color(96,153,0));
		//获取画板宽度
		int width = screenSize.width - 242;
		//获取画板高度
		int height = screenSize.height - 110;
		
		//绘制竖线
		for(int i = 25; i < width; i +=25){
			g2d.drawLine(i, 0, i, height);
		}
		
		//绘制横线
		for(int i = 25; i < height; i +=25){
			g2d.drawLine(0, i, width, i);
		}
	}

	/**
	 * 绘制路径
	 * 
	 * @param g2d
	 *            画笔
	 * @param getdPath
	 *            路径
	 */
	private void drawLine(Graphics2D g2d, Integer[] getdPath) {
		// 线条颜色切换为紫色
		g2d.setColor(Color.PINK);
		// 线条加粗至2倍
		g2d.setStroke(new BasicStroke(2f));
		// 发送消息的节点/基站在数组中的位置
		int temp = 0;
		// 第一点的x坐标，第一点的y坐标，第二点的x坐标，第二点的y坐标
		int x1, y1, x2, y2;
		
		for (int i = 1; i < datas.getdPath().length; i++) {
			
			// 当发送消息的是基站时
			if (datas.getdPath()[temp] == 0) {
				// 获取需要通讯的俩个节点的坐标
				x1 = datas.getServer().getX() + 25;
				y1 = datas.getServer().getY() + 25;
				x2 = datas.getNodes().get(datas.getdPath()[i] - 1).getX();
				y2 = datas.getNodes().get(datas.getdPath()[i] - 1).getY();
				
				// 获取偏移量数组
				int[] deviation = calculate(x1, y1, x2, y2);
				// 绘制线条
				g2d.drawLine(x1 - 2 + deviation[0], y1 - 2 + deviation[1], x2
						+ deviation[2], y2 + deviation[3]);
				
			} else if (datas.getdPath()[i] == 0) { // 当接收消息的是基站时
				
				// 获取需要通讯的俩个节点的坐标
				x1 = datas.getNodes().get(datas.getdPath()[temp] - 1).getX();
				y1 = datas.getNodes().get(datas.getdPath()[temp] - 1).getY();
				x2 = datas.getServer().getX() + 25;
				y2 = datas.getServer().getY() + 25;
				
				// 获取偏移量数组
				int[] deviation = calculate(x1, y1, x2, y2);
				// 绘制线条
				g2d.drawLine(x1 + deviation[0], y1 + deviation[1], x2 - 2
						+ deviation[2], y2 - 2 + deviation[3]);
				
			} else { // 当发送和接收消息的都是 节点时
				
				// 获取需要通讯的俩个节点的坐标
				x1 = datas.getNodes().get(datas.getdPath()[temp] - 1).getX();
				y1 = datas.getNodes().get(datas.getdPath()[temp] - 1).getY();
				x2 = datas.getNodes().get(datas.getdPath()[i] - 1).getX();
				y2 = datas.getNodes().get(datas.getdPath()[i] - 1).getY();
				
				// 获取偏移量数组
				int[] deviation = calculate(x1, y1, x2, y2);
				// 绘制线条
				g2d.drawLine(x1 + deviation[0], y1 + deviation[1], x2
						+ deviation[2], y2 + deviation[3]);
				
			}
			
			temp = i;
		}

	}

	/**
	 * 用于优化路径绘制，使两点间的路径的起点和终点始终在代表节点的圆的边上
	 * 
	 * @param x1
	 *            发送消息的节点的x坐标
	 * @param y1
	 *            发送消息的节点的y坐标
	 * @param x2
	 *            接收消息的节点的x坐标
	 * @param y2
	 *            接收消息的节点的y坐标
	 * @return 节点数组，返回优化后的起点和终点坐标
	 */
	private int[] calculate(int x1, int y1, int x2, int y2) {
		// 定义返回的坐标数组，依次为第一点的x坐标偏移量，第一点的y坐标偏移量，第二点的x坐标偏移量，第二点的y坐标偏移量
		int[] ret = new int[4];

		// 代表起始节点和终止节点的圆的半径
		int r1, r2;

		// 俩点间的横向距离和俩点间的纵向距离
		int sx = Math.abs(x2 - x1);
		int sy = Math.abs(y2 - y1);

		// 俩点间的距离
		double s = Math.sqrt(Math.pow(sx, 2) + Math.pow(sy, 2));

		if (x1 == 55 && y1 == 55) { // 如果发送消息的节点时基站
			r1 = 25;
			r2 = 10;

		} else if (x2 == 55 && y2 == 55) { // 如果接收消息的节点时基站
			r1 = 11;
			r2 = 25;
		} else {
			r1 = r2 = 11;
		}

		// 半径与总距离的比率
		double k1 = r1 / s;
		double k2 = r2 / s;

		// 计算偏移量
		int x11 = (int) (sx * k1);
		int x22 = (int) (sx * k2);
		int y11 = (int) (sy * k1);
		int y22 = (int) (sy * k2);

		// 计算x偏移方向
		if (x1 < x2) {
			ret[0] = x11;
			ret[2] = (-x22);
		} else {
			ret[0] = (-x11);
			ret[2] = x22;
		}

		// 计算y偏移方向
		if (y1 < y2) {
			ret[1] = y11;
			ret[3] = (-y22);
		} else {
			ret[1] = (-y11);
			ret[3] = y22;
		}

		return ret;
	}

	/**
	 * 处理点击事件
	 */
	public void mouseClicked(MouseEvent e) {
		// 获取系统当前时间，用于判断是否为双击
		long secondClick = System.currentTimeMillis();
		// 如果俩次点击间隔小于0.3秒则认为是双击
		if (!(secondClick - firstClick > 300)) {
			// 获取被点击的控件
			lbNodePosition = (JLabel) e.getSource();
			// 获取控件中节点/基站的编号
			int num = Integer.valueOf(lbNodePosition.getText().toString());
			// 如果是基站
			if (num == 0) {
				//弹出基站信息修改窗口
				new UpdateServerInfoDialog((MainWindow) callback, true,
						datas.getServer(), callback).setVisible(true);
			} else { // 如果是节点
				Node node = datas.getNodes().get(num - 1);
				// 如果是恶意节点
				if (node.isMaliciousNode()) {
					new UpdateMaliciousNodeInfoDialog((MainWindow) callback, true, node,
							num - 1, callback).setVisible(true);
				} else { // 如果是正常节点
					// 弹出修改窗口
					new UpdateNodeInfoDialog((MainWindow) callback, true, node,
							num - 1, callback).setVisible(true);
				}
			}
		}
		firstClick = secondClick;
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

}
