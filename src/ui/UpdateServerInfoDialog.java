package ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import bean.Server;
import service.CallbackInter;

/**
 * 基站信息修改窗口
 * 
 * @author 聂立勤
 *
 */
public class UpdateServerInfoDialog extends JDialog implements MouseListener, ActionListener {

	private static final long serialVersionUID = 6837413311258430295L;

	/**
	 * 回调接口，用于更新MainWindow中的Datas
	 */
	private CallbackInter callback;
	/**
	 * 要修改信息的节点对象
	 */
	private Server server;

	/*-------------------------控件-----------------------------*/

	// 提示信息
	private JLabel jlTitle = new JLabel("基站",JLabel.CENTER);
	// 基站坐标
	private JLabel jlLocation = new JLabel();
	
	private JLabel jlRadius = new JLabel("半  径：");
	// 基站广播半径输入框
	private JTextField jtfRadius = new JTextField(5);

	// 消息信息栏
	private JPanel messageInfo = new JPanel();
	private JLabel jlMessage = new JLabel("— — 即时消息");
	private JLabel jlMessageLog = new JLabel("— — 消息记录");
	// JLabel支持简单的HTML标记
	private JLabel jlEntityLog = new JLabel("<HTML><U>点击查看完整信息</U></HTML>");
	// 基站即时消息输入框
	private JTextArea jtaMessage = new JTextArea();
	// 基站消息记录显示框
	private JTextArea jtaMessageLog = new JTextArea();

	private JButton enter = new JButton("确定");
	private JButton reset = new JButton("重填");

	public UpdateServerInfoDialog(MainWindow parent, boolean model,
			Server server, CallbackInter callback) {
		super(parent, model);
		this.server = server;
		this.callback = callback;

		setTitle("修改基站信息");
		setBounds(550, 200, 300, 355);
		setSize(300, 355);
		setResizable(false);
		setLayout(null);

		initUI();
	}

	private void initUI() {

		jlTitle.setBounds(10, 5, 280, 15);
		jlLocation.setBounds(10, 20, 280, 15);
		jlLocation.setText("( " + server.getX() + " , " + server.getY() + " )");
		jlLocation.setHorizontalAlignment(JLabel.CENTER);
		
		jlRadius.setBounds(17, 50, 50, 15);
		jtfRadius.setBounds(63, 45, 80, 25);
		jtfRadius.setText(server.getRadius() + "");
		
		reset.setBounds(160, 45, 60, 24);
		reset.addActionListener(this);
		enter.setBounds(225, 45, 60, 24);
		enter.addActionListener(this);
		
		
		
		add(jlTitle);
		add(jlLocation);
		add(jlRadius);
		add(jtfRadius);
		add(enter);
		add(reset);
		
		initMessageInfoPanel();
	}

	/**
	 * 初始化消息面板
	 */
	private void initMessageInfoPanel() {
		messageInfo.setLayout(null);
		messageInfo.setBounds(8, 72, 280, 245);
		messageInfo.setBorder(BorderFactory.createTitledBorder("消息信息"));

		jlMessage.setBounds(10, 110, 260, 15);
		jlMessage.setHorizontalAlignment(JLabel.RIGHT);
		jlMessageLog.setBounds(170, 225, 100, 15);
		jlMessageLog.setHorizontalAlignment(JLabel.RIGHT);
		jlEntityLog.setBounds(10, 225, 160, 15);
		jlEntityLog.addMouseListener(this);

		// 滚动条，使jtaMessage和jtaMessageLog能够滚动
		JScrollPane jspMessage = new JScrollPane(jtaMessage);
		JScrollPane jspMessageLog = new JScrollPane(jtaMessageLog);

		jspMessage.setBounds(10, 20, 260, 85);
		jtaMessage.setTabSize(2);// 设置Tab键切两格
		jtaMessage.setLineWrap(true);// 激活自动换行功能
		jtaMessage.setWrapStyleWord(true);// 激活断行不断字功能
		jtaMessage.setText(server.getMessage());
		jspMessageLog.setBounds(10, 135, 260, 85);
		jtaMessageLog.setLineWrap(true);
		jtaMessageLog.setWrapStyleWord(true);
		jtaMessageLog.setEditable(false);
		jtaMessageLog.setText(server.getMessageLog());

		messageInfo.add(jspMessage);
		messageInfo.add(jspMessageLog);
		messageInfo.add(jlMessage);
		messageInfo.add(jlMessageLog);
		messageInfo.add(jlEntityLog);

		add(messageInfo);

	}
	
	/**
	 * 按钮点击事件
	 */
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton)e.getSource();
		if (button.equals(enter)){   //如果点击的是确定按钮
			//获取基站信息
			String sRadius = jtfRadius.getText().trim().toString();
			
			try{
				//数据类型转换
				double radius = Double.valueOf(sRadius);
				
				if(radius < 0){ //半径大小不符合规范时弹窗
					JOptionPane.showMessageDialog(null, "半径不能小于1（推荐在100以上）！", "Error",
							JOptionPane.ERROR_MESSAGE);
				}else{
					
					//获取消息
					String sMessage = jtaMessage.getText().trim().toString();
					
					//向server插入数据
					server.setRadius(radius);
					server.setMessage(sMessage);
					
					//通知MainWindow更改数据
					callback.updateServerInfo(server);
					
					//销毁弹窗
					dispose();
				}
				
				
				
			}catch(NumberFormatException nfe){
				JOptionPane.showMessageDialog(null, "配置信息类型有误！", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
			
		}else if (button.equals(reset)){  //如果点击的是重置按钮
			jtfRadius.setText(server.getRadius() + "");
			jtaMessage.setText(server.getMessage());
		}
		
		
	}
	
	/*------------------------查看完整信息按钮动画及事件-------------------------*/

	public void mouseClicked(MouseEvent arg0) {
		// 点击事件,弹出显示消息记录的窗口
		new MessageLogDialog((MainWindow)callback, true, server.getMessageLog()).setVisible(true);
		jlEntityLog.setForeground(null);
	}

	public void mouseEntered(MouseEvent arg0) {
		//进入控件范围时变色
		jlEntityLog.setForeground(Color.BLUE);
	}

	public void mouseExited(MouseEvent arg0) {
		//离开控件范围时变回原色
		jlEntityLog.setForeground(null);
	}

	public void mousePressed(MouseEvent arg0) {
		//按下控件时变为斜体
		jlEntityLog.setFont(new Font("微软雅黑", Font.ITALIC, 13));
	}

	public void mouseReleased(MouseEvent arg0) {
		//放开控件时变回原字体
		jlEntityLog.setFont(new Font("微软雅黑", Font.PLAIN, 13));	
	}

}
