package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import service.CallbackInter;
import bean.Node;

/**
 * 修改恶意节点信息窗口
 * 
 * @author 聂立勤
 *
 */
public class UpdateMaliciousNodeInfoDialog extends JDialog implements ActionListener, ChangeListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8337070664507770371L;
	/**
	 * 回调接口，用于更新MainWindow中的Datas
	 */
	private CallbackInter callback;
	/**
	 * 要修改信息的节点对象
	 */
	private Node node;
	/**
	 * 节点在节点集合中的位置
	 */
	private int num;

	/*------------------------------控件-------------------------------*/
	// 提示信息
	private JLabel jlTitle = new JLabel();
	// 节点坐标
	private JLabel jlLocation = new JLabel();

	// 基本信息栏
	private JPanel baseInfo = new JPanel();
	// 基本信息栏子项
	private JLabel jlPower = new JLabel("电量：", JLabel.CENTER);
	private JLabel jlConsumption = new JLabel("功耗：", JLabel.CENTER);
	private JLabel jlRadius = new JLabel("半径：", JLabel.CENTER);
	// 节点电量输入框
	private JTextField jtfPower = new JTextField(5);
	// 节点功耗输入框
	private JTextField jtfConsumption = new JTextField(5);
	// 节点半径输入框
	private JTextField jtfRadius = new JTextField(5);

	// 复选信息栏
	private JPanel radioGroupInfo = new JPanel();
	private JLabel jlState = new JLabel("修改一号节点");
	private JLabel jlNoConsumption = new JLabel("删除一号节点");
	private JLabel jlAsMaliciousNode = new JLabel("删除二号节点");
	// 启用节点勾选框
	private JCheckBox jcbState = new JCheckBox();
	// 取消耗电勾选框
	private JCheckBox jcbNoConsumption = new JCheckBox();
	// 将该节点设为恶意节点勾选框
	private JCheckBox jcbAsMaliciousNode = new JCheckBox();

	// 消息信息栏
	private JPanel messageInfo = new JPanel();
	private JLabel jlMessage = new JLabel("— — 即时消息");
	private JLabel jlMessageLog = new JLabel("— — 消息记录");
	// 节点即时消息输入框
	private JTextArea jtaMessage = new JTextArea();
	// 节点消息记录显示框
	private JTextArea jtaMessageLog = new JTextArea();

	private JButton enter = new JButton("确定");
	private JButton reset = new JButton("重填");

	/**
	 * UpdateNodeInfoWindow的构造函数
	 * 
	 * @param parent
	 *            主窗口引用
	 * @param model
	 *            弹窗模式（以对话框模式弹出，主窗口将挂起）
	 * @param node
	 *            当前选中节点
	 * @param num
	 *            当前选中节点在集合中的位置
	 * @param callback
	 *            回调接口
	 */
	public UpdateMaliciousNodeInfoDialog(MainWindow parent, boolean model, Node node,
			int num, CallbackInter callback) {
		super(parent, model);

		this.node = node;
		this.num = num;
		this.callback = callback;

		setTitle("修改节点信息");
		setBounds(500, 150, 400, 500);
		setSize(400, 500);
		setResizable(false);
		setLayout(null);

		initUI();
	}

	/**
	 * 初始化UI
	 */
	private void initUI() {

		// 标题
		jlTitle.setBounds(30, 10, 100, 20);
		jlTitle.setHorizontalAlignment(JLabel.CENTER);
		jlTitle.setText("第 " + (num + 1) + " 号 恶 意 节 点");

		// 坐标
		jlLocation.setBounds(30, 30, 100, 20);
		jlLocation.setHorizontalAlignment(JLabel.CENTER);
		jlLocation.setText("( " + node.getX() + " , " + node.getY() + " )");

		// 按钮
		reset.setBounds(212, 15, 70, 30);
		enter.setBounds(298, 15, 70, 30);
		reset.addActionListener(this);
		enter.addActionListener(this);

		add(jlTitle);
		add(jlLocation);
		add(enter);
		add(reset);

		// 初始化各面板
		initBaseInfoPanel();
		initRadioGroupInfoPanel();
		initMessageInfoPanel();

	}

	/**
	 * 初始化基本信息面板
	 */
	private void initBaseInfoPanel() {
		baseInfo.setLayout(null);
		baseInfo.setBounds(30, 60, 160, 145);
		baseInfo.setBorder(BorderFactory.createTitledBorder("基本信息"));

		jlPower.setBounds(10, 10, 65, 35);
		jlPower.enableInputMethods(false);
		jlConsumption.setBounds(10, 55, 65, 35);
		jlConsumption.enableInputMethods(false);
		jlRadius.setBounds(10, 100, 65, 35);

		jtfPower.setBounds(75, 15, 65, 25);
		jtfPower.setText("MAX");
		jtfPower.setEditable(false);

		jtfConsumption.setBounds(75, 60, 65, 25);
		jtfConsumption.setText("MAX");
		jtfConsumption.setEditable(false);

		jtfRadius.setBounds(75, 105, 65, 25);
		jtfRadius.setText("" + node.getRadius());
		jtfRadius.setEditable(false);


		baseInfo.add(jlPower);
		baseInfo.add(jlConsumption);
		baseInfo.add(jlRadius);
		baseInfo.add(jtfPower);
		baseInfo.add(jtfConsumption);
		baseInfo.add(jtfRadius);

		add(baseInfo);
	}

	/**
	 * 初始化复选信息面板
	 */
	private void initRadioGroupInfoPanel() {
		radioGroupInfo.setLayout(null);
		radioGroupInfo.setBounds(210, 60, 160, 145);
		radioGroupInfo.setBorder(BorderFactory.createTitledBorder("破坏方式"));

		jlState.setBounds(30, 10, 65, 35);
		jlNoConsumption.setBounds(30, 55, 65, 35);
		jlAsMaliciousNode.setBounds(30, 100, 65, 35);

		jcbState.setBounds(105, 15, 25, 25);
		jcbState.setSelected(node.getType() == Node.FRIST);
		jcbState.addChangeListener(this);
		jcbNoConsumption.setBounds(105, 60, 25, 25);
		jcbNoConsumption.setSelected(node.getType() == Node.SECOND);
		jcbNoConsumption.addChangeListener(this);
		jcbAsMaliciousNode.setBounds(105, 105, 25, 25);
		jcbAsMaliciousNode.setSelected(node.getType() == Node.SECOND);
		jcbAsMaliciousNode.addChangeListener(this);

		radioGroupInfo.add(jlState);
		radioGroupInfo.add(jlNoConsumption);
		radioGroupInfo.add(jlAsMaliciousNode);
		radioGroupInfo.add(jcbState);
		radioGroupInfo.add(jcbNoConsumption);
		radioGroupInfo.add(jcbAsMaliciousNode);

		add(radioGroupInfo);

	}

	/**
	 * 初始化消息面板
	 */
	private void initMessageInfoPanel() {
		messageInfo.setLayout(null);
		messageInfo.setBounds(30, 215, 340, 245);
		messageInfo.setBorder(BorderFactory.createTitledBorder("消息信息"));

		jlMessage.setBounds(10, 110, 320, 15);
		jlMessage.setHorizontalAlignment(JLabel.RIGHT);
		jlMessageLog.setBounds(10, 225, 320, 15);
		jlMessageLog.setHorizontalAlignment(JLabel.RIGHT);

		// 滚动条，使jtaMessage和jtaMessageLog能够滚动
		JScrollPane jspMessage = new JScrollPane(jtaMessage);
		JScrollPane jspMessageLog = new JScrollPane(jtaMessageLog);

		jspMessage.setBounds(10, 20, 320, 85);
		jtaMessage.setTabSize(2);// 设置Tab键切两格
		jtaMessage.setLineWrap(true);// 激活自动换行功能
		jtaMessage.setWrapStyleWord(true);// 激活断行不断字功能
		jtaMessage.setText(node.getMessage());
		jspMessageLog.setBounds(10, 135, 320, 85);
		jtaMessageLog.setLineWrap(true);
		jtaMessageLog.setWrapStyleWord(true);
		jtaMessageLog.setEditable(false);
		jtaMessageLog.setText(node.getMessageLog());

		messageInfo.add(jspMessage);
		messageInfo.add(jspMessageLog);
		messageInfo.add(jlMessage);
		messageInfo.add(jlMessageLog);

		add(messageInfo);

	}

	/**
	 * 处理按钮点击事件
	 */
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton)e.getSource();
		if(button.equals(enter)){         //按下确定按钮
			/**
			 * 异常类型，用于提示：1为电量异常，2为功耗大于电量异常，3为半径异常
			 */
			int exceptionType = 0;

			String sRadius = jtfRadius.getText().trim().toString();

			// 数据完整性校验
			if (sRadius.length() == 0) {
				JOptionPane.showMessageDialog(null, "请输入完整配置信息！", "Error",
						JOptionPane.ERROR_MESSAGE);
			} else {
				try {

					double radius = Double.valueOf(sRadius);
					
				    if (radius <= 0 ) {
						exceptionType = 3;
						throw new Exception();
					}
					
					node.setRadius(radius);
				
					if(jcbState.isSelected()){
						node.setType(Node.FRIST);
					} else if(jcbNoConsumption.isSelected()){
						//node.setType(Node);
					} else if(jcbAsMaliciousNode.isSelected()){
						node.setType(Node.SECOND);
					} else{
						exceptionType = 2;
						throw new Exception();
					}
					
					node.setPowerConsumption(0);
					
					node.setMessage(jtaMessage.getText().trim().toString());
					
					//回调更新数据
					callback.updateNodeInfo(node, num);
					
					// 销毁
					dispose();

				} catch (NumberFormatException nf) {
					JOptionPane.showMessageDialog(null, "配置信息类型有误！", "Error",
							JOptionPane.ERROR_MESSAGE);
				} catch (Exception ex) {
					switch (exceptionType) {
					case 1:
						JOptionPane.showMessageDialog(null, "请选择恶意节点工作方式！", "Error",
								JOptionPane.ERROR_MESSAGE);
						break;
					case 2:
						JOptionPane.showMessageDialog(null, "请选择恶意节点工作方式！", "Error",
								JOptionPane.ERROR_MESSAGE);
						break;
					case 3:
						JOptionPane.showMessageDialog(null, "节点广播半径不能小于1（推荐半径在100以上）！", "Error",
								JOptionPane.ERROR_MESSAGE);
						break;
					}
				}
				
			}
		} else if(button.equals(reset)){  //按下重置按钮
			jtfPower.setText(node.getPower() + "");
			jtfConsumption.setText(node.getPowerConsumption() + "");
			jtfRadius.setText(node.getRadius() + "");
			jtaMessage.setText(node.getMessage());
			jcbState.setSelected(node.isState());
			jcbAsMaliciousNode.setSelected(node.isMaliciousNode());
			if(node.getPowerConsumption() == 0)
				jcbNoConsumption.setSelected(true);
			else 
				jcbNoConsumption.setSelected(false);
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if(e.getSource() == jcbState){
			if(jcbState.isSelected()){
				jcbNoConsumption.setSelected(false);
				jcbAsMaliciousNode.setSelected(false);
			}
		} else if(e.getSource() == jcbNoConsumption){
			if(jcbNoConsumption.isSelected()){
				jcbState.setSelected(false);
				jcbAsMaliciousNode.setSelected(false);
			}
		} else if(e.getSource() == jcbAsMaliciousNode){
			if(jcbAsMaliciousNode.isSelected()){
				jcbNoConsumption.setSelected(false);
				jcbState.setSelected(false);
			}
		}
		
	}
}
