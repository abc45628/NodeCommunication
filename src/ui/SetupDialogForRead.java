package ui;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import bean.Datas;

/**
 * 查看配置信息窗口
 * 
 * @author 聂立勤
 *
 */
public class SetupDialogForRead extends JDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5152997906197233536L;
	private JTextField jNodeNum = new JTextField(3); // 节点个数输入框
	private JTextField jPowerConsumption = new JTextField(5); // 节点功耗输入框
	private JTextField jPower = new JTextField(5); // 节点电量输入框
	private JTextField jRadius = new JTextField(5); // 节点半径输入框

	private JLabel title = new JLabel("配  置  信  息", JLabel.CENTER); // 提示信息

	private JLabel num = new JLabel("数量:");
	private JLabel power = new JLabel("电量:");
	private JLabel powerConsumption = new JLabel("功耗:");
	private JLabel radius = new JLabel("半径:");
	private JLabel hint = new JLabel("Tips：以上为节点初始化信息，更改后的信息不会在此显示", JLabel.CENTER);
	
	/**
	 * 数据存储库
	 */
	private Datas datas = null;

	/**
	 * SetupWindow的构造函数
	 * @param parent 主窗口引用
	 * @param model 弹窗模式（true为对话框型弹出，主窗口将挂起）
	 * @param callback 回调接口
	 */
	public SetupDialogForRead(MainWindow parent,boolean model,Datas datas) {
		//调用父类方法使主窗口挂起
		super(parent,model);
		
		this.datas = datas;
		setTitle("节点配置信息");
		setBounds(500, 200, 400, 400);
		setSize(400, 400);
		
		
		setResizable(false);
		setLayout(null);
		
		initUI();

	}

	/**
	 * 初始化UI
	 */
	private void initUI() {

		title.setBounds(150, 15, 100, 30);

		num.setBounds(50, 70, 45, 30);
		jNodeNum.setBounds(95, 70, 80, 30);

		power.setBounds(225, 70, 45, 30);
		jPower.setBounds(270, 70, 80, 30);

		powerConsumption.setBounds(50, 170, 45, 30);
		jPowerConsumption.setBounds(95, 170, 80, 30);

		radius.setBounds(225, 170, 45, 30);
		jRadius.setBounds(270, 170, 80, 30);
		
		hint.setBounds(0, 250, 400, 30);


		add(title);
		add(num);
		add(jNodeNum);
		add(power);
		add(jPower);
		add(powerConsumption);
		add(jPowerConsumption);
		add(radius);
		add(jRadius);
		add(hint);
		
		
		jNodeNum.setText(datas.getSum() + "");
		jPower.setText(datas.getPower() + "");
		jPowerConsumption.setText(datas.getConsumption() + "");
		jRadius.setText(datas.getRadius() + "");
		
		jNodeNum.setEditable(false);
		jPower.setEditable(false);
		jPowerConsumption.setEditable(false);
		jRadius.setEditable(false);
	}
}
