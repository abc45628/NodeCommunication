package service;

import java.util.List;

import ui.MainWindow.CheckState;
import bean.Node;
import bean.Server;

public interface CallbackInter {

	// //////////////////////////左侧菜单栏接口///////////////////////////////
	/****************************** 参数 **********************************/
	/**
	 * 初始化节点网络
	 * 
	 * @param num
	 *            节点数量
	 * @param powerConsumption
	 *            节点功耗
	 * @param power
	 *            节点电量
	 * @param radius
	 *            节点广播半径
	 */
	public void setupDatas(int num, double powerConsumption, double power,
			double radius);

	/**
	 * 随机生成节点网络
	 */
	public void setupDatasRandom(CheckState checkState);

	/**
	 * 清空节点网络
	 */
	public void resetDatas();

	/****************************** 运行 **********************************/
	
	/**
	 * 在两节点间发送消息
	 * @param souceNode 源节点编号
	 * @param targetNode 目标节点编号
	 * @param message 消息
	 * @param path 路径
	 */
	public void sendMessage(int souceNode,int targetNode,String message,Integer[] path);
	
	/**
	 * 在两节点间发送加密消息
	 * @param souceNode 源节点编号
	 * @param targetNode 目标节点编号
	 * @param message 消息
	 * @param encrpty 加密消息
	 * @param path 路径
	 */
	public void sendMessage(int souceNode,int targetNode,String message,byte[] encrpty, Integer[] path);
	
	/**
	 * 屏蔽节点网络中的恶意节点
	 * @param mNodes 恶意节点列表
	 */
	public void deleteMaliciousNode(List<Integer> mNodes);

	/****************************** 路径 **********************************/
	// ///////////////////////////路径管理接口///////////////////////////////
	
	/**
	 * 生成某个节点到基站的路径
	 * @param num 节点编号
	 * @param type 是否开启多路径选择
	 */
	public void buildPath(int num,boolean type);
	
	/**
	 * 获取路径
	 */
	public void getPath();
	

	// ///////////////////////////绘制窗口接口///////////////////////////////
	/**
	 * 普通节点参数修改
	 * 
	 * @param node
	 *            当前节点对象
	 * @param num
	 *            节点编号
	 */
	public void updateNodeInfo(Node node, int num);

	/**
	 * 基站参数修改
	 * 
	 * @param server
	 *            基站对象
	 */
	public void updateServerInfo(Server server);
	
	

}
