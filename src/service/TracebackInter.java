package service;

/**
 * 溯源算法接口
 *
 */
public interface TracebackInter {
	/**
	 * 通知屏蔽网路中的恶意节点
	 * @param num 节点跳数
	 */
	public void confirmMaliciousNode(int hop);
	
	/**
	 * 通知屏蔽网络中的恶意节点
	 */
	public void confirmMaliciousNode();
}
