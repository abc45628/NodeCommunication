package service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;

import bean.Datas;
import bean.Message;
import bean.Node;
import bean.Server;

public class CommunicateServiceImpl {

  /**
   * 节点表
   */
  private List<Node> nodes = null;

  /**
   * 基站对象
   */
  private Server server = null;

  /**
   * 用于产生随机数以完成概率事件
   */
  Random random = new Random();

  public CommunicateServiceImpl(Datas datas) {
    this.nodes = datas.getNodes();
    this.server = datas.getServer();
  }

  /**
   * 从将源节点的消息发送至基站
   * 
   * @param souceNode
   *          源节点编号
   * @param targetNode
   *          目标节点编号
   * @param message
   *          要传递的消息
   * @param path
   *          传递消息的路径
   */
  public void sendMessage(int souceNode, int targetNode, String message, Integer[] path) {
    if (path != null) {
      if (message != null && !"".equals(message)) {

        nodes.get(path[0] - 1).setMessage(message);

        // 新建一个消息实体
        Message body = new Message(souceNode, targetNode, message, path);
        Node node = null;

        // 遍历路径以模拟发送消息
        for (int i = 0; i < path.length; i++) {
          // 获取节点编号
          int num = path[i];
          if (num != 0) {
            // 获取节点实体
            node = nodes.get(num - 1);
          } else {
        	//当节点为第零个节点时跳数置0
        	body.setHop(0);
          }

          // 获取随机数以完成概率事件
          int ran = random.nextInt(100);
          if (node != null) {
            // 设置消息本体
            node.setMessageLog(node.getMessageLog() + getTime() + "\nFrom " + body.getSourceNode() + "  to  "
                + body.getTargetNode() + "\nMessage:" + body.getMessage() + "\n\n");

              // 当节点为恶意节点时，修改消息实体中的信息
              if (node.isMaliciousNode()) {
            	switch(node.getType()){
            		//第一种方法，修改第一节点的编号
            		case Node.FRIST:  
            			//当恶意节点处于尚未标记第二个点的位置时
            			if (body.getFristNode() != -1 && body.getNextNode() == -1){ 
            				//将第一节点的编号随机修改为路径中节点的编号
            				int temp = path[random.nextInt(body.getPath().length - 1)];
            				//随机生成的节点编号不能是当前节点编号
            				while (temp == body.getFristNode()){
            					temp = path[random.nextInt(body.getPath().length - 1)];
            				}
            				body.setFristNode(temp);
            				//跳数增加
            				body.setHop(body.getHop() + 1);
            				//标记修改
            				body.setRight(false);
            				//当恶意节点处于标记的第二个点之后时不增加跳数
            			} else if(body.getFristNode() != -1){
            				int temp = path[random.nextInt(body.getPath().length - 1)];
            				while (temp == body.getFristNode()){
            					temp = path[random.nextInt(body.getPath().length - 1)];
            				}
            				body.setFristNode(temp);
            				//标记修改
            				body.setRight(false);
            			} else if(body.getFristNode() == -1 && body.getNextNode() == -1){
            				body.setHop(body.getHop() + 1);
            			}
            			break;
            		//第二种方法，位于俩点之后时删除第一个点
            		case Node.SECOND:           			
            			if (body.getFristNode() != -1 && body.getNextNode() != -1){
            				body.setFristNode(-1);
            				//标记修改
            				body.setRight(false);
            			} else if(body.getFristNode() != -1 && body.getNextNode() == -1){
            				//位于第二个标记节点标号之前不采取任何操作但增加跳数并增加标记
            				body.setHop(body.getHop() + 1);
            				//标记修改
            				body.setRight(false);
            			} else if(body.getFristNode() == -1 && body.getNextNode() == -1){
            				//位于俩点之前不采取任何操作但增加跳数
            				body.setHop(body.getHop() + 1);
            			}
            			break;
            		//第三种方法，位于俩点之后时删除第二个点，此时由于第二个点会被删除，所以一定会增加跳数
            		case Node.THIRD:
            			//位于俩点之后
            			if(body.getFristNode() != -1 && body.getNextNode() != -1){
            				body.setNextNode(-1);
            				body.setHop(body.getHop() + 1);
            				//标记修改
            				body.setRight(false);
            			//位于俩点之间
            			} else if(body.getFristNode() != -1 && body.getNextNode() == -1){
            				body.setHop(body.getHop() + 1);
            				//标记修改
            				body.setRight(false);
            			//位于俩点之前
            			} else {
            				body.setHop(body.getHop() + 1);
            			}
            		default:
            			break;
            	}  
                body = node.changeMessage(body);
              } else {
                // 当为普通节点时，将自己的编号加入消息头部
            	// 当符合发生概率，并且没有节点已经将自己的编号放入消息实体中，并且这个节点并不是目标节点的时候
                if (ran < node.getOdds() && body.getFristNode() == -1 && body.getNextNode() ==-1 && i != (path.length - 1)) {
                    body.setFristNode(node.getNum());
                    body.setHop(body.getHop() + 1);
                } else if(body.getFristNode() != -1 && body.getNextNode() ==-1 && i != (path.length - 1)){
                	body.setNextNode(node.getNum());
                	body.setHop(body.getHop() + 1);
                } else if(body.getFristNode() == -1 && body.getNextNode() ==-1 && i != (path.length - 1)){
                	body.setHop(body.getHop() + 1);
                } else if(i == (path.length - 1)){
                	server.receive(body);
                }
              }
            
              node = null;
          }
        }
      }else {
          JOptionPane.showMessageDialog(null, "消息不能为空！", "Error", JOptionPane.ERROR_MESSAGE);
          }
    }
  }

  public void sendMessage(int souceNode, int targetNode, String message, byte[] encryptMessage, Integer[] path) {
    if (path != null) {
      if (message != null) {

        nodes.get(path[0] - 1).setMessage(message);

        // 新建一个消息实体
        Message body = new Message(souceNode, targetNode, encryptMessage, path);
        Node node = null;

        // 遍历路径以模拟发送消息
        for (int i = 0; i < path.length; i++) {
          // 获取节点编号
          int num = path[i];
          if (num != 0) {
            // 获取节点实体
            node = nodes.get(num - 1);
          }

          // 获取随机数以完成概率事件
          int ran = random.nextInt(100);
          if (node != null) {
            // 设置消息本体
            node.setMessageLog(node.getMessageLog() + getTime() + "\nFrom " + body.getSourceNode() + "  to  "
                + body.getTargetNode() + "\nMessage:" + body.getMessage() + "\n\n");

         // 当节点为恶意节点时，修改消息实体中的信息
            if (node.isMaliciousNode()) {
            	switch(node.getType()){
        		//第一种方法，修改第一节点的编号
        		case Node.FRIST:  
        			//当恶意节点处于尚未标记第二个点的位置时
        			if (body.getFristNode() != -1 && body.getNextNode() == -1){ 
        				//将第一节点的编号随机修改为路径中节点的编号
        				int temp = path[random.nextInt(body.getPath().length - 1)];
        				//随机生成的节点编号不能是当前节点编号
        				while (temp == body.getFristNode()){
        					temp = path[random.nextInt(body.getPath().length - 1)];
        				}
        				body.setFristNode(temp);
        				//跳数增加
        				body.setHop(body.getHop() + 1);
        				//标记修改
        				body.setRight(false);
        				//当恶意节点处于标记的第二个点之后时不增加跳数
        			} else if(body.getFristNode() != -1){
        				int temp = path[random.nextInt(body.getPath().length - 1)];
        				while (temp == body.getFristNode()){
        					temp = path[random.nextInt(body.getPath().length - 1)];
        				}
        				body.setFristNode(temp);
        				//标记修改
        				body.setRight(false);
        			} else if(body.getFristNode() == -1 && body.getNextNode() == -1){
        				body.setHop(body.getHop() + 1);
        			}
        			break;
        		//第二种方法，删除第一个点
        		case Node.SECOND:  
        			//位于俩点之后
        			if (body.getFristNode() != -1 && body.getNextNode() != -1){
        				body.setFristNode(-1);
        				//标记修改
        				body.setRight(false);
        			} else if(body.getFristNode() != -1 && body.getNextNode() == -1){
        				body.setFristNode(-1);
        				//标记修改
        				body.setHop(body.getHop() + 1);
        				//标记修改
        				body.setRight(false);
        			} else if(body.getFristNode() == -1 && body.getNextNode() == -1){
        				//位于俩点之前不采取任何操作但增加跳数
        				body.setHop(body.getHop() + 1);
        			}
        			break;
        		//第三种方法，位于俩点之后时删除第二个点，此时由于第二个点会被删除，所以一定会增加跳数
        		case Node.THIRD:
        			//位于俩点之后
        			if(body.getFristNode() != -1 && body.getNextNode() != -1){
        				body.setNextNode(-1);
        				body.setHop(body.getHop() + 1);
        				//标记修改
        				body.setRight(false);
        			//位于俩点之间
        			} else if(body.getFristNode() != -1 && body.getNextNode() == -1){
        				body.setHop(body.getHop() + 1);
        				//标记修改
        				body.setRight(false);
        			//位于俩点之前
        			} else {
        				body.setHop(body.getHop() + 1);
        			}
        		default:
        			break;
        	}  
              body = node.changeMessage(body);
            } else {
              // 当为普通节点时，将自己的编号加入消息头部
          	// 当符合发生概率，并且没有节点已经将自己的编号放入消息实体中，并且这个节点并不是目标节点的时候
              if (ran < node.getOdds() && body.getFristNode() == -1 && body.getNextNode() ==-1 && i != (path.length - 1)) {
                  body.setFristNode(node.getNum());
                  body.setHop(body.getHop() + 1);
              } else if(body.getFristNode() != -1 && body.getNextNode() ==-1 && i != (path.length - 1)){
              	body.setNextNode(node.getNum());
              	body.setHop(body.getHop() + 1);
              } else if(body.getFristNode() == -1 && body.getNextNode() ==-1 && i != (path.length - 1)){
              	body.setHop(body.getHop() + 1);
              } else if(i == (path.length - 1)){
              	server.receive(body);
              }
            }
          
            node = null;
        }
      }
    }else {
        JOptionPane.showMessageDialog(null, "消息不能为空！", "Error", JOptionPane.ERROR_MESSAGE);
        }
  }

  }

  /**
   * 获取系统当前时间
   */
  private String getTime() {
    // 记录时间
    Timestamp ts = new Timestamp(System.currentTimeMillis());
    String sendTime = "";
    DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    try {
      sendTime = sdf.format(ts);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return sendTime;
  }

}
