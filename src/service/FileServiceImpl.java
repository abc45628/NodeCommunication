package service;

import com.alibaba.fastjson.JSON;

import bean.Datas;
import bean.DatasSerialProcess;

public class FileServiceImpl {
  public FileServiceImpl() {}

  public String saveDatas(Datas datas) {
    if (datas == null) { return null; }
    String json = JSON.toJSONString(datas,new DatasSerialProcess());
    return json;
  }

  public Datas openDatas(String datasText) {
    Datas datas = JSON.parseObject(datasText, Datas.class);
    return datas;
  }
}
