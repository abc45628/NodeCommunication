package service;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.swing.JOptionPane;

import utils.DijkstraMain;
import bean.Datas;
import bean.Node;
import bean.Server;

/**
 * 路径管理器，用于生成路径,获取各种条件下的路径</br> 算法要求：</br> 1. 在一个无向连通图中求出两个给定点之间的所有路径；</br> 2.
 * 在所得路径上不能含有环路或重复的点；</br> 算法思想描述：</br> 1.
 * 整理节点间的关系，为每个节点建立一个集合，该集合中保存所有与该节点直接相连的节点（不包括该节点自身）；</br> 2.
 * 定义两点一个为起始节点，另一个为终点，求解两者之间的所有路径的问题可以被分解为如下所述的子问题：</br> 对每一
 * 个与起始节点直接相连的节点，求解它到终点的所有路径（路径上不包括起始节点）得到一个路径集合，
 * 将这些路径集合相加就可以得到起始节点到终点的所有路径；依次类推就可以应用递归的思想，层层递归直到终点，
 * 若发现希望得到的一条路径，则转储并打印输出；若发现环路，或发现死路，则停止寻路并返回； </br> 3.
 * 用栈保存当前已经寻到的路径（不是完整路径）上的节点，
 * 在每一次寻到完整路径时弹出栈顶节点；而在遇到从栈顶节点无法继续向下寻路时也弹出该栈顶节点，从而实现回溯。</br>
 * 
 * @author 聂立勤
 *
 */
public class PathServiceImpl implements Runnable {

	/**
	 * 数据存储器
	 */
	Datas datas = null;

	/**
	 * 代表基站的节点
	 */
	Node nodeForServer = null;

	/**
	 * 节点表
	 */
	List<Node> nodes = null;

	/**
	 * 路径表
	 */
	List<List<Integer>> paths = null;

	/**
	 * 路径计数器，用于记录当前绘制的是第几条路径
	 */
	int pathCount = 0;

	/**
	 * 当前路径
	 */
	List<Integer> path = null;
	
	/**
	 * 源节点编号
	 */
	int sourceNum = -1;
	
	/**
	 * 路径最长数量
	 */
	int maxCount = -1;
	
	/**
	 * 屏幕参数
	 */
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

	/**
	 * 临时保存路径节点的栈
	 */
	public Stack<Node> stack = new Stack<Node>();
	
	/**
	 * 将耗时操作放置到线程中进行
	 */
	//private Thread thread = new Thread(this);
	
	/**
	 * 最初的起始节点
	 */
	private Node currentNode = null;
	
	/**
	 * 源节点
	 */
	private Node startNode = null;
	
	/**
	 * 终止节点
	 */
	private Node endNode = null;
	
	private DijkstraMain dijkstra = null;

	public PathServiceImpl(Datas datas) {
		if (datas != null) {
			this.datas = datas;
			Server server = datas.getServer();
			nodeForServer = new Node(server.getNum(), server.getX(), server.getY(),
					server.getRadius(), server.getPower(),
					server.getPower_consumption());

			nodeForServer.setNodes(server.getNodes());
			nodeForServer.setDvalues(server.getDvalues());

			nodes = datas.getNodes();
			
			//节点中最小半径
			double min = Double.MAX_VALUE;
			for(Node temp : nodes){
				if(temp.getRadius() < min){
					min = temp.getRadius();
				}
			}
			
			int width = screenSize.width - 242;
			//最大节点限制定义规则为：路径最长需要屏幕总长/节点最小半径的值与其俩俩间刚好无法连接而需要第三个点桥接的第三点的个数
			maxCount  = (int)(width / min) * 2 - 1; 
			
			
		}

	}

	public Integer[] getDijkstraPath(int sourceNum, int targetNum){
		Integer[] ret = null;
		dijkstra = new DijkstraMain(datas);
		if(dijkstra != null){
			int[] temp = dijkstra.getPath(sourceNum,targetNum);
			if(temp != null){
				ret = new Integer[temp.length];
				for(int i = 0;i < temp.length; i ++){
					ret[temp.length - 1 - i] = temp[i];
				}
			}
				
			
		}
			
		
		return ret;
	}
	
	/**
	 * 获取俩点之间的所有路径
	 * 
	 * @param sourceNum
	 *            源节点编号
	 * @param targetNum
	 *            目标节点编号
	 */
	public Integer[] getPaths(int sourceNum, int targetNum) {
		this.sourceNum = sourceNum;
		Integer[] ret = null;
		List<Integer> path = null;
		paths = new LinkedList<List<Integer>>();
		if (sourceNum == 0){ // 源节点为基站
			currentNode = nodeForServer;
			startNode = nodeForServer;
			endNode = nodes.get(targetNum - 1);
		}
			
		else if (targetNum == 0){ // 目标节点为基站
			currentNode = nodes.get(sourceNum - 1);
			startNode = nodes.get(sourceNum - 1);
			endNode = nodeForServer;
		}
		
		go(currentNode,null,startNode,endNode);
		// 初始化计数器
		pathCount = 0;
		//ProgressBarUtil.show((JFrame)null, thread,"路径生成中，请稍候...");
		
		if(paths != null && paths.size() != 0){
			path = paths.get(0);
			ret = new Integer[path.size()];
			ret = path.toArray(ret);
		}
		
		return ret;
	}

	/**
	 * 获取下一条路径
	 * 
	 * @return 整型数组
	 */
	public Integer[] getNextPath() {
		Integer[] ret = null;

		if (paths != null && paths.size() != 0){
			// 当计数器没有超出路径数组大小时可以获取
			if (pathCount < paths.size()) {
				List<Integer> path = paths.get(pathCount);
				ret = new Integer[path.size()];
				ret = path.toArray(ret);

				pathCount++;
			} else {
				JOptionPane.showMessageDialog(null, "没有更多路径！", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}

		return ret;
	}

	/**
	 * 获取上一条路径
	 * 
	 * @return 整型数组
	 */
	public Integer[] getPrePath() {
		Integer[] ret = null;

		pathCount--;
		if (paths != null && paths.size() != 0){
			// 当计数器不小于-1时可以获取
			if (pathCount > -1) {
				List<Integer> path = paths.get(pathCount);
				ret = new Integer[path.size()];
				ret = path.toArray(ret);

				pathCount--;
			} else {
				JOptionPane.showMessageDialog(null, "已经是第一条路径！", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		
		pathCount++;
		return ret;
	}

	/**
	 * 获取电量最足路径
	 * 
	 * @return 路径数组
	 */
	public Integer[] getTheMostPowerfulPath() {
		Integer[] ret = null;

		if (paths != null && paths.size() != 0){
			// 最大总电量
			double max = Double.MIN_VALUE;
			// 最大总电量的路径在路径列表中的位置
			int count = 0;
			// 路径总长
			double power = 0;
			// 当前路径
			List<Integer> list = null;

			for (int i = 0; i < paths.size(); i++) {
				list = paths.get(i);
				for (int j = 0; j < list.size(); j++) {
					int num = list.get(j);
						if (num != 0) // 基站不参与耗电
							power += nodes.get(num - 1).getPower();
				}
				
				if(power > max){
					max = power;
					count = i;
				}

			}
			
			list = paths.get(count);
			ret = new Integer[list.size()];
			ret = list.toArray(ret);

			// 释放内存
			list = null;
		}
		
		
		

		return ret;
	}

	/**
	 * 获取路程最短路径
	 * 
	 * @return 路径数组
	 */
	public Integer[] getTheShortestPath() {
		Integer[] ret = null;

		if(paths != null && paths.size() != 0){
			// 最小路径总长
			int min = Integer.MAX_VALUE;
			// 最小总长的路径在路径列表中的位置
			int count = 0;
			// 路径总长
			int distance = 0;
			// 距离表
			Map<Integer, Integer> map = null;
			// 当前路径
			List<Integer> list = null;

			for (int i = 0; i < paths.size(); i++) {
				list = paths.get(i);
				distance = 0;
				for (int j = 0; j < list.size(); j++) {
					int num = list.get(j);
					// 当为路径的起始节点时，并不存在前驱节点，只需要获取其中的map
					if (j == 0) {
						if (num == 0) // 如果是基站
							map = nodeForServer.getDvalues();
						else
							map = nodes.get(num - 1).getDvalues();
					} else {
						if(map != null){
							int tem = map.get(num);
							distance += tem;
						}					
						if (num == 0) // 如果是基站
							map = nodeForServer.getDvalues();
						else
							map = nodes.get(num - 1).getDvalues();
					}
				}

				if (distance < min) {
					min = distance;
					count = i;
				}
			}

			list = paths.get(count);
			ret = new Integer[maxCount];
			ret = list.toArray(ret);

			// 释放内存
			list = null;
			map = null;
		}
		
		return ret;
	}

	/*************************************** 以下是寻路算法 *******************************************/

	/**
	 * 判断节点是否在栈中
	 * 
	 * @param node
	 *            节点
	 * @return true为在栈中
	 */
	public boolean isNodeInStack(Node node) {
		Iterator<Node> it = stack.iterator();
		while (it.hasNext()) {
			Node node1 = (Node) it.next();
			if (node == node1)
				return true;
		}
		return false;
	}
	
	/**
	 * 判断已找到的路径中是否存在该节点
	 * @param node 节点
	 * @return
	 */
	public boolean isNodeInPaths(Node node){
		boolean ret = false;
		int num = node.getNum();
		if (num != 0 && num != sourceNum){
			if(paths.size() > 0){
				for(List<Integer> list:paths){
					for(Integer value:list){
						int temp = value + 1;
						if(temp == (num + 1)){
							ret = true;
							return ret;
						}
					}
				}
			}
		}
		
		return ret;
	}

	/**
	 * 此时栈中的节点组成一条所求路径，转储并打印输出
	 */
	public void showAndSavePath() {
		Object[] o = stack.toArray();
		path = new LinkedList<Integer>();
		for (int i = 0; i < o.length; i++) {
			Node nNode = (Node) o[i];
			path.add(nNode.getNum());
			if (i < (o.length - 1))
				System.out.print(nNode.getNum() + "->");
			else
				System.out.print(nNode.getNum());
		}

		// 转储
		paths.add(path);

		System.out.println("\n");

	}

	/**
	 * 寻找路径的方法 </br>
	 * @param cNode 当前的起始节点currentNode </br>
	 * @param pNode 当前起始节点的上一节点previousNode </br>
	 * @param sNode 最初的起始节点startNode </br>
	 * @param eNode 终点endNode </br>
	 * @return
	 */
	public boolean go(Node cNode, Node pNode, Node sNode, Node eNode) {
		Node nNode = null;
		/* 如果符合条件判断说明出现环路，不能再顺着该路径继续寻路，返回false */
		if (cNode != null && pNode != null && cNode == pNode)
			return false;
		
		if (isNodeInPaths(cNode))
			return false;

		if (cNode != null) {
			int i = 0;
			/* 起始节点入栈 */
			stack.push(cNode);
			/* 如果该起始节点就是终点，说明找到一条路径 */
			if (cNode == eNode) {
				/* 转储并打印输出该路径，返回true */
				showAndSavePath();
				return true;
			}
			/* 如果不是,继续寻路 */
			else {
				/*
				 * 从与当前起始节点cNode有连接关系的节点集中按顺序遍历得到一个节点 作为下一次递归寻路时的起始节点
				 */
				int num = cNode.getNodes().get(i);
				if (num == 0)
					nNode = nodeForServer;
				else
					nNode = nodes.get(num - 1);

				while (nNode != null) {
					/*
					 * 如果nNode是最初的起始节点或者nNode就是cNode的上一节点或者nNode已经在栈中 ， 说明产生环路
					 * ，应重新在与当前起始节点有连接关系的节点集中寻找nNode
					 */
					if (pNode != null
							&& (nNode == sNode || nNode == pNode || isNodeInStack(nNode))) {
						i++;
						if (i >= cNode.getNodes().size())
							nNode = null;
						else {

							int num1 = cNode.getNodes().get(i);
							if (num1 == 0)
								nNode = nodeForServer;
							else
								nNode = nodes.get(num1 - 1);
						}

						continue;
					}
					
					/* 以nNode为新的起始节点，当前起始节点cNode为上一节点，递归调用寻路方法 */
					if (stack.size() < maxCount && go(nNode, cNode, sNode, eNode))/* 递归调用 */
					{
						/* 如果找到一条路径，则弹出栈顶节点 */
						stack.pop();
						if(stack.size() > 1)
							return true;
					} else if (stack.size() > maxCount){
						break;
					}
					
					
					/* 继续在与cNode有连接关系的节点集中测试nNode */
					i++;
					if (i >= cNode.getNodes().size())
						nNode = null;
					else {

						int num2 = cNode.getNodes().get(i);
						if (num2 == 0)
							nNode = nodeForServer;
						else
							nNode = nodes.get(num2 - 1);
					}
				}
				/*
				 * 当遍历完所有与cNode有连接关系的节点后， 说明在以cNode为起始节点到终点的不相交路径已经全部找到
				 */
				stack.pop();
				return false;
			}
		} else
			return false;
	}

	public void run() {
		go(currentNode,null,startNode,endNode);
		// 初始化计数器
		pathCount = 0;
	}
}
