package service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.alibaba.fastjson.JSON;

import bean.Protocol;

public class ProtocolServiceImpl {
  /**
   * 要读取的协议文件
   */
  private File file = null;
  /**
   * 记录了协议文件的每一行
   */
  private List<Protocol> rowList = null;

  /**
   * @throws FileNotFoundException
   *           判定(!file.exists() || !file.isFile())
   */
  public ProtocolServiceImpl(File file) throws FileNotFoundException {
    if (!file.exists() || !file.isFile()) { throw new FileNotFoundException(); }
    this.file = file;
  }

  /*
   * 验证协议正确性并读取，只读取正确部分
   */
  public List<Protocol> read() throws FileNotFoundException {
    Scanner scanner = new Scanner(file,"utf-8");
    rowList = new ArrayList<Protocol>();
    while (scanner.hasNextLine()) {
      String currentLine = scanner.nextLine();
      System.out.println(currentLine);
      Protocol protocol;
      try {
        protocol = JSON.parseObject(currentLine, Protocol.class);
      } catch (Exception e) {
        continue;
      }
      rowList.add(protocol);
    }
    scanner.close();
    return rowList;
  }

  @Override
  public String toString() {
    StringBuffer sBuffer=new StringBuffer();
    for(Protocol p:rowList){
      sBuffer.append(p.toText());
    }
    return "读取文件" + file+"\n 脚本内容\n"+sBuffer.toString();
  }

  // **************get()set()************//
  public List<Protocol> getRowList() {
    return rowList;
  }
}
