package service;

/**
 * 焦点管理接口
 * 
 * @author 聂立勤
 *
 */
public interface FousableInter {

	/**
	 * 设置是否允许获取焦点
	 * @param state 请求
	 */
	public void setFousable(boolean state);
}
