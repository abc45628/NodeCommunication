package bean;

import org.apache.commons.codec.digest.DigestUtils;

public class Message {
	
	/**
	 * 源节点
	 */
	private int sourceNode = -1;
	
	/**
	 * 目标节点
	 */
	private int targetNode = -1;
	
	/**
	 * 以一定概率加入的节点的编号
	 */
	private int fristNode = -1;
	
	/**
	 * 邻接上上一跳加入编号的节点的节点的编号
	 */
	private int nextNode = -1;
	
	/**
	 * 节点的跳数，当节点对包进行操作时时添加
	 */
	private int hop = -1;
	
	/**
	 * 判断是否修改的标识
	 */
	private boolean isRight = true;
	
	/**
	 * 完整的通讯路径
	 */
	private Integer[] path = null;
	
	/**
	 * 要发送的消息
	 */
	private String message = "";
	
	/**
	 * MD5校验码
	 */
	private String hex = "";
	/**
	 * 密文消息
	 */
	private byte[] ecryptMessage = null;

	public Message(int sourceNode,int targetNode,String message,Integer[] path){
		this.sourceNode = sourceNode;
		this.targetNode = targetNode;
		this.message = message;
		this.path = path;
		this.hex = DigestUtils.md5Hex(message);
	}
	
	public Message(int sourceNode,int targetNode,byte[] ecryptMessage,Integer[] path){
		this.sourceNode = sourceNode;
		this.targetNode = targetNode;
		this.ecryptMessage = ecryptMessage;
		this.path = path;
		this.message = ecryptMessage.toString();
		this.hex = DigestUtils.md5Hex(ecryptMessage);
	}
	
	public int getSourceNode() {
		return sourceNode;
	}

	public void setSourceNode(int sourceNode) {
		this.sourceNode = sourceNode;
	}

	public int getTargetNode() {
		return targetNode;
	}

	public void setTargetNode(int targetNode) {
		this.targetNode = targetNode;
	}

	public int getFristNode() {
		return fristNode;
	}

	public void setFristNode(int fristNode) {
		this.fristNode = fristNode;
	}

	public int getNextNode() {
		return nextNode;
	}

	public void setNextNode(int nextNode) {
		this.nextNode = nextNode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer[] getPath() {
		return path;
	}

	public void setPath(Integer[] path) {
		this.path = path;
	}

	public byte[] getEcryptMessage() {
		return ecryptMessage;
	}

	public void setEcryptMessage(byte[] ecryptMessage) {
		this.ecryptMessage = ecryptMessage;
	}

	public String getHex() {
		return hex;
	}

	public int getHop() {
		return hop;
	}

	public void setHop(int hop) {
		this.hop = hop;
	}

	public boolean isRight() {
		return isRight;
	}

	public void setRight(boolean isRight) {
		this.isRight = isRight;
	}
	
	

}
