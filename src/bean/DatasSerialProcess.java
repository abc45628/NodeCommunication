package bean;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.PropertyPreFilter;

public class DatasSerialProcess implements PropertyPreFilter {

  @Override
  public boolean apply(JSONSerializer serializer, Object object, String name) {
    //将不需要序列化的字段或变量作为一个case语句，同时返回false
    switch (name) {
      case "pathType":;
      case "callback":;
      case "isShowTable":
        return false;
      default:
        return true;
    }
  }

}
