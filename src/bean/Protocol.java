package bean;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 协议中每一行的模板
 * 
 * @author ZZZZZ
 *
 */
public class Protocol {
  /**
   * 起始节点
   */
  @JSONField(ordinal = 0)
  int node;

  /**
   * 要发送到节点的消息
   */
  @JSONField(ordinal = 1)
  String message;

  public Protocol(int node, String message) {
    this.node = node;
    this.message = message;
  }

  public Protocol() {}

  @Override
  public String toString() {
    return "Protocol [node=" + node + ", message=" + message + "]";
  }
  
  public String toText() {
    return "node=" + node + ", message=" + message + "\n";
  }

  public int getNode() {
    return node;
  }

  public void setNode(int node) {
    this.node = node;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
