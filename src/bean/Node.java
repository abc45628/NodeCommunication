package bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class Node implements Serializable{
	private static final long serialVersionUID = 6651725926323589352L;

	/**
	 * 编号
	 */
	private int num;
	
	/**
	 * 状态
	 */
	private boolean state = true;
	
	/**
	 * X坐标
	 */
	private int x;
	
	/**
	 * Y坐标
	 */
	private int y;
	
	/**
	 * 半径
	 */
	private double radius;
	
	/**
	 * 电量
	 */
	private double power;
	
	/**
	 * 功耗
	 */
	private double powerConsumption;
	
	/**
	 * 添加自己id的概率
	 */
	private double odds = 30;
	
	/**
	 * 邻接表
	 */
	private List<Integer> nodes = null;
	
	/**
	 * 以邻接表中的节点的编号为键值存储距离
	 */
	private Map<Integer,Integer> dvalues = null;
	
	/**
	 * 用于保存通讯记录
	 */
	private String messageLog = "";
	
	/**
	 * 用于发送的临时数据
	 */
	private String message = "";
	
	/**
	 * 是否是恶意节点
	 */
	private boolean isMaliciousNode = false;
	
	/**
	 * 如果该节点是恶意节点时破坏信息的方式，默认为添加字段
	 */
	private int type = FRIST;
	
	/**
	 * 首个邻接节点
	 */
	public NextNode firstNode = null;  
	
	public Node() {}
	
	/**
	 * 
	 * @param num
	 *            编号
	 * @param x
	 *            X坐标
	 * @param y
	 *            Y坐标
	 * @param radius
	 *            半径
	 * @param power
	 *            电量
	 * @param power_consumption
	 *            功耗
	 */
	public Node(int num, int x, int y, double radius, double power, double powerConsumption) {
		this.num = num;
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.power = power;
		this.powerConsumption = powerConsumption;
		this.dvalues = new HashMap<Integer, Integer>();
		this.nodes = new LinkedList<Integer>();
	}
	
	/**
	 * 用于在连接结束或中断时消除Message
	 **/
	public void deleteMessage() {
		this.message = null;
	}
	
	// 以下是get、set
	public int getNum() {
		return num;
	}
	
	public void setNum(int num) {
		this.num = num;
	}
	
	public boolean isState() {
		return state;
	}
	
	public void setState(boolean state) {
		this.state = state;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getPower() {
		return power;
	}
	
	public void setPower(double power) {
		this.power = power;
	}
	
	public double getPowerConsumption() {
		return powerConsumption;
	}

	public void setPowerConsumption(double powerConsumption) {
		this.powerConsumption = powerConsumption;
	}
	
	public Map<Integer, Integer> getDvalues() {
		return dvalues;
	}

	public void setDvalues(Map<Integer, Integer> nodes) {
		this.dvalues = nodes;
		try {
			finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public String getMessageLog() {
		return messageLog;
	}
	
	public void setMessageLog(String messageLog) {
		this.messageLog = messageLog;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isMaliciousNode() {
		return isMaliciousNode;
	}

	public void setMaliciousNode(boolean isMaliciousNode) {
		this.isMaliciousNode = isMaliciousNode;
	}

	public double getOdds() {
		return odds;
	}

	public void setOdds(double odds) {
		this.odds = odds;
	}

	public List<Integer> getNodes() {
		return nodes;
	}

	public void setNodes(List<Integer> nodes) {
		this.nodes = nodes;
		try {
			finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public NextNode getFirstNode() {
		return firstNode;
	}

	public void setFirstNode(NextNode firstNode) {
		this.firstNode = firstNode;
		try {
			finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
  public String toString() {
    return "Node [num=" + num + ", state=" + state + ", x=" + x + ", y=" + y + ", radius=" + radius + ", power=" + power
        + ", powerConsumption=" + powerConsumption + ", odds=" + odds + ", nodes=" + nodes + ", dvalues=" + dvalues
        + ", messageLog=" + messageLog + ", message=" + message + ", isMaliciousNode=" + isMaliciousNode + ", type="
        + type + ", firstNode=" + firstNode + "]";
  }
	

  /**
	 * 修改消息实体的信息
	 * @param message 消息实体类
	 * @param type 修改方式
	 */
	public Message changeMessage(Message message){
		Message ret = null;
		String before = message.getMessage();
		//switch(type){
		//case FRIST:
			if(message.getEcryptMessage() != null){
				byte[] beforeMsg = message.getEcryptMessage();
				byte[] afterMsg = new byte[beforeMsg.length + 3];
				for(int i = 0;i < afterMsg.length; i ++){
					if(i < beforeMsg.length){
						afterMsg[i] = beforeMsg[i];
					} else{
						afterMsg[i] = 1;
					}
				}
				
				message.setEcryptMessage(afterMsg);
				message.setMessage(afterMsg.toString());
				
			}else{
				before = before + " 存在恶意节点!";
				message.setMessage(before);
			}
			//break;
//		case SECOND:
//			if(message.getEcryptMessage() != null){
//				byte[] beforeMsg = message.getEcryptMessage();
//				byte[] afterMsg = new byte[beforeMsg.length];
//				for(int i = 0;i < afterMsg.length; i ++){
//						afterMsg[i] = (byte) (beforeMsg[i] == 0? 1:0);
//				}
//				message.setEcryptMessage(afterMsg);
//				message.setMessage(afterMsg.toString());
//			} else{
//				if(before.length() < 3){
//					message.setMessage("");
//				} else {
//					message.setMessage(before.substring(0, before.length() - 3));
//				}
//			}	
//			break;
//		case 3:
//			message.setEcryptMessage(null);
//			message.setMessage("你被劫持了!");
//			break;
//		}
		
		ret = message;
		
		return ret;
	}
	
	/**
	 * 第一种修改方式，修改第一点的编号
	 */
	public static final int FRIST = 0;
	
	/**
	 * 第二种修改方式，位于俩点之后时删除第一点
	 */
	public static final int SECOND = 1;

	/**
	 * 第三种修改方式，位于俩点时候时删除第二点
	 */
	public static final int THIRD = 2;
	
}