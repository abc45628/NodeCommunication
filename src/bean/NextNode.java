package bean;

import java.io.Serializable;

public class NextNode implements Serializable {
  private static final long serialVersionUID = -3542288401976202704L;
  /**
   * 邻接结点的索引。
   */
  public int nodeNum;
  /**
   * 结点与其邻接结点之间的边的权值。
   */
  public int edgeWeight;
  /**
   * 结点所在邻接链表中的下一个邻接结点及其信息。
   */
  public NextNode nextNode = null;

  /**
   * 通过邻接结点的索引及其与所在邻接链表表头结点之间的边的权值来构造一个新的邻接结点对象。
   * 
   * @param node
   *          邻接结点索引。
   * @param weight
   *          邻接结点与所在邻接链表表头结点之间的边的权值。
   */
  public NextNode(int node, int weight) {
    nodeNum = node;
    edgeWeight = weight;
  }

  public NextNode() {}
}
