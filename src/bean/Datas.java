package bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;

import service.CallbackInter;
import service.TracebackInter;
import utils.BuildAdjList;
import utils.BuildAdjListForDijkstra;

/**
 * 数据储存对象
 * 
 * @author 聂立勤
 **/
public class Datas implements Serializable,TracebackInter {
	private static final long serialVersionUID = -7344880083759331260L;

	/**
	 * 邻接表的引用，通过nodes可访问整张图的所有信息。若图不存在，则nodes为null。
	 */
	private ArrayList<Node> nodes = null;

	/**
	 * 邻接表的引用,基站邻接表
	 */
	private Server dServer = null;

	/**
	 * 恶意节点集合
	 */
	private List<Integer> mNodes = null;

	/**
	 * 路径数组，值为编号，下标为0的点是发送消息的点
	 */
	private Integer dPath[] = null;

	/**
	 * 路径选择模式
	 */
	private static int pathType = 0;

	/**
	 * 操作接口
	 */
	transient private CallbackInter callback;

	/**
	 * 捕获时暂停
	 */
	transient private boolean stop = false;

	/**
	 * 是否显示网格
	 */
	transient private boolean isShowTable = false;

	/**
	 * 配置信息-节点总数
	 */
	private int sum = -1;

	/**
	 * 配置信息-电量
	 */
	private double power = -1;

	/**
	 * 配置信息-功耗
	 */
	private double consumption = -1;

	/**
	 * 配置信息-半径
	 */
	private double radius = -1;

	public Datas() {}
	
	/**
	 * Datas的构造函数，初始化节点及基站信息
	 * 
	 * @param sum
	 *            节点总数
	 * @param power
	 *            节点电量
	 * @param consumption
	 *            节点功耗
	 * @param radius
	 *            节点广播半径
	 * @param width
	 *            画板宽度
	 * @param height
	 *            画板高度
	 */
	public Datas(int sum, double power, double consumption, double radius, int width, int height,
			CallbackInter callback) {

		this.sum = sum;
		this.power = power;
		this.consumption = consumption;
		this.radius = radius;

		this.nodes = new ArrayList<Node>();
		this.mNodes = new LinkedList<Integer>();
		this.callback = callback;

		// 初始化数据
		setupDatas(sum, power, consumption, radius, width, height);
	}

	/**
	 * 初始化节点网络
	 * 
	 * @param sum
	 *            节点总数
	 * @param power
	 *            节点电量
	 * @param consumption
	 *            节点功耗
	 * @param radius
	 *            节点广播半径
	 * @param width
	 *            画板宽度
	 * @param height
	 *            画板高度
	 */
	private void setupDatas(int sum, double power, double consumption, double radius, int width, int height) {
		// 画板宽度
		int w = width;
		// 画板高度
		int h = height;
		// 节点间间距
		int distance = 40;
		/****************************** 生成节点及基站 *******************************/
		// 生成基站，位置固定在左上角
		int tempX = 30;
		int tempY = 30;
		dServer = new Server(tempX, tempY, radius, this);

		// 生成节点
		for (int i = 0; i < sum; i++) {
			// 判断是否可插入
			boolean isInsertable = true;
			// 随机生成坐标
			tempX = (int) (Math.random() * (w - 40)) + 20;
			tempY = (int) (Math.random() * (h - 40)) + 20;
			// 节点去重,保证节点间距，机制为当节点间间距小于distance或离基站过近时本次生成无效，重新生成
			for (Node nodeList : nodes) {
				if ((Math.abs(nodeList.getX() - tempX) < distance && Math.abs(nodeList.getY() - tempY) < distance)
						|| (tempX < 100 && tempY < 100)) {
					i--;
					isInsertable = false;
					break;
				}
			}
			// 如果符合条件
			if (isInsertable) {
				// 节点编号从1开始生成节点
				nodes.add(new Node(i + 1, tempX, tempY, radius, power, consumption));
			}
		}

		// 异常检查
		for (Node nodeList : nodes) {
			// 如果存在节点与基站距离过近，则重新生成
			if ((Math.abs(nodeList.getX() - dServer.getX()) < 50 && Math.abs(nodeList.getY() - dServer.getY()) < 50)) {
				// 清空节点及基站
				dServer = null;
				nodes = new ArrayList<Node>();
				// 重新生成节点网络
				setupDatas(sum, power, consumption, radius, width, height);
				break;
			}
		}

		/****************************** 节点及基站生成完毕 *******************************/
		// 生成邻接表
		BuildAdjList.toAdjNodes(nodes, dServer);
		BuildAdjListForDijkstra.toAdj_Nodes(nodes, dServer);

	}

	public ArrayList<Node> getNodes() {
		return nodes;
	}

	public void setNodes(ArrayList<Node> nodes) {
		this.nodes = nodes;
	}

	public Server getServer() {
		return dServer;
	}

	public void setServer(Server server) {
		for(int i = 0;i < nodes.size(); i++){
			nodes.get(i).setRadius(server.getRadius());
		}
		this.dServer = server;
	}

	public void setmNodes(List<Integer> mNodes) {
		this.mNodes = mNodes;
	}

	public List<Integer> getmNodes() {
		return mNodes;
	}

	public Integer[] getdPath() {
		return dPath;
	}

	public void setdPath(Integer[] dPath) {
		this.dPath = dPath;
	}

	public static int getPathType() {
		return pathType;
	}

	public static void setPathType(int path) {
		pathType = path;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public boolean isShowTable() {
		return isShowTable;
	}

	public void setShowTable(boolean isShowTable) {
		if (!this.isShowTable) {
			JOptionPane.showMessageDialog(null, "网格中每一格的边长均为25", "PLAIN", JOptionPane.PLAIN_MESSAGE);
		}
		this.isShowTable = isShowTable;
	}

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power) {
		this.power = power;
	}

	public double getConsumption() {
		return consumption;
	}

	public void setConsumption(double consumption) {
		this.consumption = consumption;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	/**
	 * 只有最短路径
	 */
	public static final int ONLYONE = 1;
	/**
	 * 自动选择路径
	 */
	public static final int AUTO = 2;
	/**
	 * 最短路径
	 */
	public static final int THE_SHORTEST = 2;
	/**
	 * 电量最足路径
	 */
	public static final int THE_MOST_POWERFUL = 3;
	/**
	 * 上一条路径
	 */
	public static final int PRE = 4;
	/**
	 * 下一条路径
	 */
	public static final int NEXT = 5;

	@Override
	public void confirmMaliciousNode(int hop) {
		mNodes.add(dPath[hop]);	
		callback.deleteMaliciousNode(mNodes);
	}

	@Override
	public void confirmMaliciousNode() {
		mNodes.add(dPath[dPath.length - 1]);	
		callback.deleteMaliciousNode(mNodes);
	}

}
