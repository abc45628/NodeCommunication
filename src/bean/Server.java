package bean;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

import service.TracebackInter;
import utils.Constants;
import utils.EncryptUtil;

/**
 * 基站对象
 **/
public class Server implements Serializable{
	private static final long serialVersionUID = -6691337741317546229L;

	/**
	 * 编号
	 */
	private int num = 0;
	
	/**
	 * X坐标
	 */
	private int x;
	
	/**
	 * Y坐标
	 */
	private int y;
	/**
	 * 状态
	 */
	private boolean state = true;
	
	/**
	 * 半径
	 */
	private double radius;
	
	/**
	 * 电量
	 */
	private double power;
	
	/**
	 * 功耗
	 */
	private double power_consumption;
	
	/**
	 * 是否执行耗电操作，不执行为false，执行为true
	 */
	private boolean power_reduce = true;
	
	/**
	 * 用于保存通讯记录
	 */
	private String messageLog = "";
	
	/**
	 * 用于发送的临时数据
	 */
	private String message = "";
	
	/**
	 * 已找到的恶意节点列表
	 */
	private List<Integer> mNodes ;
	
	/**
	 * 邻接表
	 */
	private List<Integer> nodes = null;
	
	/**
	 * 还原中的路径
	 */
	private List<Integer> tPath = null;
	
	/**
	 * 路径中节点的总数
	 */
	private int count = -1;
	
	/**
	 * 发送消息至基站的节点编号
	 */
	private int sourceNode = -1;
	
	/**
	 * 以邻接表中的节点的编号为键值存储距离
	 */
	private Map<Integer,Integer> dvalues = null;
	
	/**
	 * 首个邻接节点
	 */
	public NextNode firstNode = null;  
	
	/**
	 * 溯源算法回调接口
	 */
	transient private TracebackInter traceback;
	
	public Server() {}
	
	/**
	 * 
	 * @param x
	 *            X坐标
	 * @param y
	 *            Y坐标
	 * @param radius
	 *            半径
	 */
	public Server(int x, int y, double radius,TracebackInter traceback) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.power = 1;
		this.power_consumption = 0;
		this.traceback = traceback;
		mNodes = new ArrayList<Integer>();
		this.dvalues = new HashMap<Integer, Integer>();
		this.nodes = new LinkedList<Integer>();
		this.mNodes = new LinkedList<Integer>();
	}
	
	/**
	 * 
	 * @param x
	 *            X坐标
	 * @param y
	 *            Y坐标
	 * @param radius
	 *            半径
	 * @param power
	 *            电量
	 */
	public Server(int x, int y, double radius, double power) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.power = power;
		mNodes = new ArrayList<Integer>();
		this.dvalues = new HashMap<Integer, Integer>();
		this.nodes = new LinkedList<Integer>();
		this.mNodes = new LinkedList<Integer>();
	}
	
	/**
	 * 
	 * @param x
	 *            X坐标
	 * @param y
	 *            Y坐标
	 * @param radius
	 *            半径
	 * @param power
	 *            电量
	 * @param power_consumption
	 *            功耗
	 */
	public Server(int x, int y, double radius, double power, double power_consumption) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.power = power;
		this.power_consumption = power_consumption;
		mNodes = new ArrayList<Integer>();
		this.dvalues = new HashMap<Integer, Integer>();
		this.nodes = new LinkedList<Integer>();
		this.mNodes = new LinkedList<Integer>();
	}
	
	/**
	 * 接收消息
	 * @param message
	 */
	public void receive(Message message){
		if(message.getEcryptMessage() != null){
			byte[] msg = message.getEcryptMessage();
			String hex = DigestUtils.md5Hex(message.getEcryptMessage());
			String content = "";
			if(hex.equals(message.getHex())){
				content = EncryptUtil.decrypt(msg, Constants.KEY);
			} else {
				content = "消息已被破坏！";
			}
			
			setMessageLog(getMessageLog() + getTime()
					+ "\nFrom " + message.getSourceNode() + "  to  "
					+ message.getTargetNode() + "\nMessage:" + content
					+ "\n\n");
		}else{
			String hex = DigestUtils.md5Hex(message.getMessage());
			String content = "";
			if(hex.equals(message.getHex())){
				content = message.getMessage();
			} else {
				content = "消息已被破坏！";
			}
			setMessageLog(getMessageLog() + getTime()
					+ "\nFrom " + message.getSourceNode() + "  to  "
					+ message.getTargetNode() + "\nMessage:" + content
					+ "\n\n");
		}
		
		traceback(message);
	}
	
	/**
	 * 捕获路径中的恶意节点
	 * @param message 消息实体
	 */
	public void traceback(Message message){
		//当源节点改变时，清除记录中的数据，重新开始溯源
		if (sourceNode != message.getSourceNode()){		
			//更改源节点编号
			sourceNode = message.getSourceNode();
			//清空还原中的路径，路径中至少存在俩个点（不包含基站）
			tPath = new LinkedList<Integer>();
			tPath.add(sourceNode);
			tPath.add(-1);
			//路径中节点的数量置空
			count = -1;
		}
		
		//获取边
		int firstNode = message.getFristNode();
		int nextNode = message.getNextNode();
		//获取跳数
		int hop = message.getHop();
		//判断路径总数是否已知
		//如果未知
		if(count == -1){
			//判断第二点标记是否为空
			//如果为空，则路径中节点总数为跳数+1
			if(nextNode == -1 && message.isRight()){
				count = hop + 1;
				//扩充路径队列
				expand(count);
			} else if(hop > tPath.size()){
				expand(hop + 1);
			}
		}
		
		//获取标记以判断边是否为正确的边
		//如果是正确的边，则之间加入路径中
		if(message.isRight()){
			//如果路径中不存在则加入其中
			//不存在第二节点编号，则检查第一节点
			if(nextNode == -1){
				//如果第一节点的编号不在路径中则加入其中，否则不加人
				if(tPath.get(hop - 1) == -1)
					tPath.set(hop - 1, firstNode);
			} else {
				//如果第二节点存在并且不在路径队列中，则加入其中
				if(tPath.get(hop) == -1)
					tPath.set(hop, nextNode);
				if(tPath.get(hop - 1) == -1)
					tPath.set(hop - 1, firstNode);
			}
		//如果被恶意节点操作过，那么一定有一个点的跳数是错误的而另一个点的跳数是正确的
		} else {
			//当第一节点不存在时，第二节点一定为正确
			if(firstNode == -1 && nextNode != -1){
				if(tPath.get(hop) == -1)
					tPath.set(hop, nextNode);
			//当俩点都存在时，进行验证，找出错误点并记录正确点，如果无法验证则舍弃
			} else if(firstNode != -1 && nextNode != -1){
				//当俩点都已在路径中则无需验证，都不在则无法验证
				//当路径中有第二点没有第一点时
				if(tPath.get(hop - 1) == -1 && tPath.get(hop) != -1){
					//当路径中的点已同跳数的第二点不符时，说明第一点为正确点
					if(tPath.get(hop) != nextNode)
						//因为不符的情况下为第二点被删除后重新添加，因此第一点跳数为第二点前俩跳
						tPath.set(hop - 2, firstNode);
				} else if(tPath.get(hop - 1) != -1 && tPath.get(hop) == -1){
					//当路径中的点已同跳数的第一点不符时，说明第二点为正确点
					if(tPath.get(hop - 1) != firstNode)
						tPath.set(hop, nextNode);
				}				
			//当第一点存在，第二点不存在时，说明恶意节点就是临近基站的那一点
			} else if(firstNode != -1 && nextNode == -1){
				traceback.confirmMaliciousNode();
			}
		}	
		
		//检查路径，当路径中只有一个点未知时，说明它是恶意节点
		if(count != -1){
			int sum = 0;
			int num = -1;
			for(int i = 0; i < tPath.size(); i ++){
				if(tPath.get(i) == -1){
					sum ++ ;
					num = i;
				}
				if(sum > 1)
					break;
			}
			if(sum == 1){
				traceback.confirmMaliciousNode(num);
			}				
		}
	}
	
	/**
	 * 扩增还原中的路径队列
	 */
	private void expand(int count){
		List<Integer> temp = new LinkedList<Integer>();
		for (int i = 0; i < count; i++){
			if(i < tPath.size()){
				temp.add(tPath.get(i));
			} else 
				temp.add(-1);			
		}
		tPath = temp;
	}
		
	
	/**
	 * 获取系统当前时间
	 */
	private String getTime() {
		// 记录时间
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		String sendTime = "";
		DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			sendTime = sdf.format(ts);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sendTime;
	}
	
	// 以下是get、set
	public int getNum() {
		return num;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public boolean isState() {
		return state;
	}
	
	public void setState(boolean state) {
		this.state = state;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getPower() {
		return power;
	}
	
	public void setPower(double power) {
		this.power = power;
	}
	
	public double getPower_consumption() {
		return power_consumption;
	}
	
	public void setPower_consumption(double power_consumption) {
		this.power_consumption = power_consumption;
	}
	
	public boolean isPower_reduce() {
		return power_reduce;
	}
	
	public void setPower_reduce(boolean power_reduce) {
		this.power_reduce = power_reduce;
	}
	
	public List<Integer> getmNodes() {
		return mNodes;
	}

	public void setmNodes(List<Integer> mNodes) {
		this.mNodes = mNodes;
	}

	public String getMessageLog() {
		return messageLog;
	}
	
	public void setMessageLog(String messageLog) {
		this.messageLog = messageLog;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public Map<Integer, Integer> getDvalues() {
		return dvalues;
	}

	public void setDvalues(Map<Integer, Integer> nodes) {
		this.dvalues = nodes;
		try {
			finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Integer> getNodes() {
		return nodes;
	}

	public void setNodes(List<Integer> nodes) {
		this.nodes = nodes;
		try {
			finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public NextNode getFirstNode() {
		return firstNode;
	}

	public void setFirstNode(NextNode firstNode) {
		this.firstNode = firstNode;
		try {
			finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
